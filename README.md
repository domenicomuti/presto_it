# Presto.it

### Requisiti
- Laravel 6

- PHP 7.3.10

- Fileinfo PHP Extension

- GD2 PHP Extension

- Exif PHP Extension

- PDO SQLite PHP Extension

- SQLite3 PHP Extension

- PhpRedis

- Redis Server

- Node.js

- Laravel Echo Server

  https://github.com/tlaverdure/laravel-echo-server

- MySQL 5.7 (o altri DB supportati da Laravel)

### Per provare l'app

`composer install`

`npm install`

`npm run dev`

`cp .env.example .env`

`php artisan key:generate`

`vi .env` (BROADCAST_DRIVER=redis)

`php artisan storage:link`

`php artisan migrate:fresh --seed` 

Verrà automaticamente creato un utente con privilegi di admin e revisor.

email: *admin@admin.com* | password: *password*

Tutti gli annunci creati automaticamente saranno senza foto.

Abilitare *Google Vision API* e *Google Translate* dalla console di *Google Cloud* e inserire le proprie credenziali tramite il file json fornito da Google, rinominandolo *google_credential.json* e inserendolo nella cartella root del progetto (in mancanza di questo file i Jobs *GoogleDetectFaces*, *GoogleVisionLabelImage*, *GoogleVisionSafeSearchImage* e *TranslateLabels* non funzioneranno).

###### Avviare

`redis-server`

`php artisan queue:work`

`php artisan serve`

`laravel-echo-server start`

---

*Presto.it* è una web app completamente responsive scritta in *Laravel 6* e *Bootstrap 4*, clone del famoso sito di compravendita Subito.it (realizzata per soli scopi didattici).

Con questa app è possibile visualizzare ed inserire annunci, ricercare per categoria e anche per luogo di pubblicazione, contattare gli inserzionisti (WIP) e infine avere una personale lista di annunci preferiti.

------

L'inserimento di annunci è possibile solo per gli utenti autenticati. Si potranno inserire quindi titolo, descrizione, prezzo, categoria, luogo ed infine anche 5 immagini (facoltative).
Ogni immagine verrà opportunamente croppata e ridimensionata in varie dimensioni, alle quali verrà automaticamente aggiunto il logo di Presto.
Dal momento in cui l'utente inserirà una nuova immagine, avrà 5 minuti di tempo per confermare.
Dopo i 5 minuti apparirà a video un alert che lo avviserà che la sessione è scaduta e verrà reindirizzato verso la dashboard. Tutte le eventuali immagini temporanee caricate verranno automaticamente eliminate.
Questa funzionalità è gestita dal server dal lato backend mediante Websocket Socket.IO.

> vedi *InsertAdController.php*, *DropzoneController.php*, *DropzoneScript.js*

------

Ogni utente potrà eliminare oppure modificare il proprio annuncio cambiando tutti i campi disponibili, comprese le immagini.
La modifica delle immagini **non sarà distruttiva** fino a quando l'utente non avrà confermato il tutto cliccando sul pulsante *Modifica annuncio*.
Questo vuol dire che se verranno effettuate modifiche sulle immagini, e se non verrà confermato il tutto,  resteranno visibili sull'annuncio le sole immagini originali.
Come per l'inserimento degli annunci, vale la stessa logica che dopo 5 minuti dall'inserimento della prima immagine la sessione scade e vengono eliminate tutte le immagini temporanee.

> vedi *DropzoneModifyController.php*, *DropzoneModifyScript.js* e *HomeController.php*

------

Sarà possibile fare ricerche globalmente oppure restringerle per categoria e/o luogo di pubblicazione.
Si potranno fare ricerche anche senza scrivere alcuna stringa ma selezionando solo la categoria desiderata e/o il luogo. Compariranno di conseguenza tutti gli annunci di una determinata categoria e/o di un determinato luogo.

------

Sulla *Welcome Page*, sui *Risultati Ricerca* e sulle *viste dei singoli annunci* è presente l'icona di un cuore, cliccando su di essa si potrà inserire (o rimuovere) l'annuncio nella personale lista di Preferiti. 

L'aggiunta e la rimozione dei Preferiti tramite l'apposita icona avviene in **real-time** mediante chiamate asincrone **AJAX**, il sistema quindi si aggiornerà in automatico senza che la pagina venga ricaricata.

> Vedi *favorites.js*, *FavoritesController.php*

Dal menu utente sarà possibile quindi accedere alla lista dei propri annunci preferiti, con la possibilità di rimuoverli o visualizzarli.

------

Ogni utente autenticato potrà richiedere la possibilità di diventare *revisore* annunci (menu utente / impostazioni).

Gli utenti revisori potranno, mediante l'apposito pannello, visualizzare gli annunci appena inseriti da altri utenti, modificare la categoria con una più opportuna, ed infine approvare l'annuncio oppure respingerlo.
È prevista la possibilità di recuperare annunci respinti per errore tramite il cestino.

L'utente *Admin* può approvare o rimuovere revisori dal suo pannello Admin.

------

L'app inoltre utilizza l'API *Google Vision*.
Tramite questa API ogni immagine caricata verrà opportunamente classificata tramite IA per aiutare il revisore a riconoscere immagini violente, con contenuti per adulti o razzisti.
Inoltre restituirà dei tag che identificheranno oggetti contenuti nell'immagine; infine effettuerà un riconoscimento facciale e coprirà gli eventuali volti trovati.

------

L'utente potrà registrarsi all'applicazione in modo classico tramite il form registrazione, oppure tramite l'apposito pulsante **Registrati con Facebook**, in questo modo avverrà una chiamata all'API di Facebook  tramite il protocollo OAuth 2.0 che provvederà a recuperare *nome utente*, *email* ed andrà a compilare automaticamente il form, l'utente a questo punto dovrà solo inserire la password scelta.

Verrà poi inviato all'indirizzo email un messaggio contenente un link che permetterà all'utente di confermare la propria registrazione. Per testare la registrazione occorre quindi impostare nel file *.ENV* un provider *SMTP*.

**N.B.** per poter utilizzare correttamente la funzionalità di registrazione con Facebook occorre inserire nel file *.ENV* le chiavi

FACEBOOK_CLIENT_ID=

FACEBOOK_CLIENT_SECRET=

e valorizzarle con i dati del proprio client, opportunamente creati tramite gli strumenti per sviluppatori forniti da Facebook.

------

L'app è tradotta in *Italiano*, *Inglese* e *Spagnolo*. All'avvio viene letta la lingua del browser, se non è nessuna delle tre lingue supportate viene impostata come default la lingua Italiana, con la possibilità di cambiare lingua in un secondo momento. Molta cura è stata posta nel tradurre anche tutte le viste fornite da Laravel per l'autenticazione, oltre che le mail per il recupero password e conferma della registrazione, ed infine la pagina errore 404 personalizzata.