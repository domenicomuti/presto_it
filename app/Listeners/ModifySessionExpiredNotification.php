<?php

namespace App\Listeners;

use App\Events\ModifySessionExpired;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;

class ModifySessionExpiredNotification
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  ModifySessionExpired  $event
     * @return void
     */
    public function handle(ModifySessionExpired $event)
    {
        //
    }
}
