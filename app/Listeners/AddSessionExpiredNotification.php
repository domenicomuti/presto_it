<?php

namespace App\Listeners;

use App\Events\AddSessionExpired;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;

class AddSessionExpiredNotification
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  AddSessionExpired  $event
     * @return void
     */
    public function handle(AddSessionExpired $event)
    {
        //
    }
}
