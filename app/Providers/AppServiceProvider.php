<?php

namespace App\Providers;

use App\Region;
use App\Category;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        if(DB::connection()->getDatabaseName()!="forge"){
            if(Schema::hasTable('categories') && Schema::hasTable('regions')) {
                View::share('categories', Category::all());
                View::share('regions', Region::all());
            }
        }
    }
}
