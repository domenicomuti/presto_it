<?php

namespace App;

use App\Ad;
use App\AdImage;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;

class AdImage extends Model
{
    protected $fillable = ['file','ad_id'];

    protected $casts = [
        'labels_en' => 'array',
        'labels_it' => 'array',
        'labels_es' => 'array'
    ];
    
    public function ad(){
        return $this->belongsTo(Ad::class);
    }

    static public function getUrlByFilePath($filePath, $w = null, $h = null) 
    {
        if(!$w && !$h) {
            return $filePath;
        }
    
        $path = dirname($filePath);
        $filename = basename($filePath);
    
        $file = "{$path}/crop{$w}x{$h}_{$filename}";
    
        return $file;
    }

    public function getStorageUrl($w = null, $h = null){
        $url = AdImage::getUrlByFilePath($this->file, $w, $h);
        return Storage::url($url);
    }

    public function deleteFile($w = null, $h = null){
        $url = AdImage::getUrlByFilePath($this->file, $w, $h);
        Storage::delete($url);
    }
}