<?php

namespace App;

use App\AdImage;
use Laravel\Scout\Searchable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Ad extends Model
{
    use SoftDeletes;
    use Searchable;

    public function category() {
        return $this->belongsTo('App\Category');
    }

    public function user() {
        return $this->belongsTo('App\User');
    }

    public function region() {
        return $this->belongsTo('App\Region');
    }

    public function images() {
        return $this->hasMany(AdImage::class);
    }

    public function is_favored_by() {
        return $this->belongsToMany('App\User', 'favorites_pivot');
    }

    protected $fillable = ['title','description','price','category_id','region_id','user_id'];

    public function searchableAs() {
        return 'ads_index';
    }

    public function toSearchableArray() {
        $array = [
            'id'=> $this->id,
            'title'=> $this->title,
            'description'=> $this->description
        ];
        return $array;
    }

    static public function toBeRevisioned() {
        $count = Ad::where('published',false)->count();
        if ($count >= 1000)
            return round($count/1000, 1) . "k";
        else
            return $count;
    }
}