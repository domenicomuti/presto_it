<?php

namespace App\Http\Requests;

use Illuminate\Support\Facades\Auth;
use Illuminate\Foundation\Http\FormRequest;

class InsertAdValidation extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        /* RITORNA TRUE SOLO SE UTENTE E' AUTENTICATO */
        return Auth::check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title'=>'required|max:60',
            'description'=>'required|max:600',
            'price'=>'required|numeric|lte:99999999|gte:1|integer',
            'category_id'=>'required',
            'region_id'=>'required'
        ];
    }
}