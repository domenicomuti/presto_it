<?php

namespace App\Http\Middleware;

use App\Ad;
use Closure;
use Illuminate\Support\Facades\Auth;

class AllowUserToFavorites
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (Auth::id() == $request->user_id && Ad::find($request->ad_id)){
            return $next($request);
        }
        
        return response()->json('User not allowed or Ad not found');
    }
}