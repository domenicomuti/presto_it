<?php

namespace App\Http\Middleware;

use App\Ad;
use Closure;

class CheckPublished
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (Ad::find($request->id)->published){
            return $next($request);
        }
        return redirect()->back();
    }
}
