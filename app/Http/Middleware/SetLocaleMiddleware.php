<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\App;

class SetLocaleMiddleware

{
    public function handle($request, Closure $next)
    {
        if (!session('locale')) {
            $browserLocale = isset($_SERVER['HTTP_ACCEPT_LANGUAGE']) ? substr($_SERVER["HTTP_ACCEPT_LANGUAGE"],0,2) : '';

            switch($browserLocale){
                case 'en':
                case 'es':
                    $locale = $browserLocale;
                    break;
                default:
                    $locale = 'it';
            }
            session()->put('locale', $locale);
        }else{
            $locale = session('locale');
        }

        App::setLocale($locale);
        return $next($request);
    }
}
