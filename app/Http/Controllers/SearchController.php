<?php

namespace App\Http\Controllers;

use \App\Ad;
use Illuminate\Http\Request;
use App\Http\Requests\SearchValidator;
use Illuminate\Pagination\LengthAwarePaginator;

class SearchController extends Controller
{
    public function FullTextSearch(Request $req) {
        $category = $req->category;
        $region = $req->region;
        $query = strtolower($req->input('query'));

        if ($query == ''){
            $searchResults = Ad::with(['user', 'category', 'region', 'images']) // Eager Loading
                ->where('published', true)
                ->when($category, function($q, $category){ // Clausola condizionale
                    return $q->where('category_id', $category);
                })
                ->when($region, function($q, $region){ // Clausola condizionale
                    return $q->where('region_id', $region);
                })
                ->get()
                ->sortByDesc('id');
        }
        else{
            $searchResults = Ad::search($query)
            ->query(function($query){
                return $query->with(['user', 'category', 'region', 'images']);
            }) // Eager Loading
            ->where('published', true)
            ->when($category, function($q, $category){ // Clausola condizionale
                return $q->where('category_id', $category);
            })
            ->when($region, function($q, $region){ // Clausola condizionale
                return $q->where('region_id', $region);
            })
            ->get() // TRASFORMO IN COLLECTION
            ->sortByDesc('id');
        }
            
        /* La trasformazione dei risultati dell ricerca in collection è stata necessaria
        in quanto di default il metodo orderBy non funziona correttamente con il Paginator.
        E' stato quindi necessario impostare un custom paginator che possa lavorare con le collection.
        (il Paginator di default non riconosce le Collections) */

        /* Custom Paginator da usare con Collections */
        $perPage=10;
        $page=LengthAwarePaginator::resolveCurrentPage();
        $searchResults = new LengthAwarePaginator(
            $searchResults->forPage($page, $perPage),
            $searchResults->count(),
            $perPage,
            $page,
            $options = []
        );
        $searchResults->withPath('');

        return view('search.results',compact('searchResults','query','category','region'));
    }
}