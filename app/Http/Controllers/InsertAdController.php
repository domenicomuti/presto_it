<?php

namespace App\Http\Controllers;

use \App\Ad;
use App\AdImage;
use \App\Category;
use App\Jobs\AdToRevise;
use App\Jobs\ResizeImage;
use Illuminate\Http\Request;
use App\Jobs\TranslateLabels;
use App\Jobs\GoogleDetectFaces;
use App\Jobs\GoogleVisionLabelImage;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use App\Jobs\GoogleVisionSafeSearchImage;
use \App\Http\Requests\InsertAdValidation;

class InsertAdController extends Controller
{
    public function __construct(){
        $this->middleware(['auth','verified']);
    }

    public function insert(Request $req){
        if ($req->old('secret'))
            $secret=$req->old('secret');
        else
            $secret = base_convert(sha1(uniqid(mt_rand())),16,36);

        return view('insert.ad', compact('secret'));
    }

    public function insert_post(InsertAdValidation $req){
        $title = $req->title;
        $description = $req->description;
        $price = $req->price;
        $category_id = $req->category_id;
        $region_id = $req->region_id;
        $user_id = Auth::id();

        $ad = Ad::create(compact('title','description','price','category_id','region_id','user_id'));

        $secret = $req->secret;

        $addedFiles = session()->get("images.{$secret}", []);
        $deletedFiles = session()->get("removedimages.{$secret}", []);

        $files = array_diff($addedFiles, $deletedFiles);
        
        foreach ($files as $file) {
            $filename = basename($file);
            $new_path = "public/ads/{$ad->id}/{$filename}";
            Storage::move($file, $new_path);
            $file = $new_path;

            $ad_image = AdImage::create(['file'=>$file, 'ad_id'=>$ad->id]);

            GoogleVisionSafeSearchImage::dispatch($ad_image->id);
            GoogleVisionLabelImage::dispatch($ad_image->id);
            GoogleDetectFaces::dispatch($ad_image->id);
            ResizeImage::dispatch($file,120,120);
            ResizeImage::dispatch($file,300,300);
            ResizeImage::dispatch($file,600,450);
            ResizeImage::dispatch($file); // aggiungi watermark a immagine originale
            TranslateLabels::dispatch($ad_image->id);
        }

        AdToRevise::dispatch($ad);

        Storage::deleteDirectory("public/temp/{$secret}");

        return redirect()->route('insert.thanks');
    }

    public function insert_thanks(){
        return view('insert.thanks');
    }
}