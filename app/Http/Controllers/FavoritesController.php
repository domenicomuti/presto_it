<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class FavoritesController extends Controller
{
    public function __construct(){
        $this->middleware(['auth','verified']);
    }

    public function favorites_home(){
        $ads = Auth::user()->favorite_ads()->get()->sortBy('id');
        return view('home.favorites',compact('ads'));
    }

    public function add(Request $req){
        $ad_id = $req->ad_id;
        $user_id = $req->user_id;

        User::find($user_id)
            ->favorite_ads()
            ->syncWithoutDetaching([$ad_id]); // Usato al posto di attach per evitare possibili duplicati
        
        return response()->json("Add OK");
    }

    public function delete(Request $req){
        $ad_id = $req->ad_id;
        $user_id = $req->user_id;

        User::find($user_id)
            ->favorite_ads()
            ->detach($ad_id);
        
        return response()->json("Delete OK");
    }
}