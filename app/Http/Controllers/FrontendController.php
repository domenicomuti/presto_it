<?php

namespace App\Http\Controllers;

use App\Ad;
use App\User;
use App\Mail\WorkMail;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use App\Http\Requests\WorkWithUsValidator;

class FrontendController extends Controller
{
    public function welcome() {
        $ads = Ad::with(['region','images','category','is_favored_by'])
            ->where('published',true)
            ->orderBy('id', 'desc')
            ->take(8)
            ->get();
        return view('frontend.welcome', compact('ads'));
    }

    public function ad_details($id) {
        $ad = Ad::find($id);
        /*
            Passo categoria e regione, in questo modo sulla barra di ricerca
            a scomparsa da smartphone verrà automaticamente selezionata
            categoria e regione dell'annuncio cliccato.
        */
        $category = $ad->category_id;
        $region = $ad->region_id;
        return view('frontend.ad_details', compact('ad','category','region'));
    }

    public function locale($locale){
        session()->put('locale', $locale);
        return redirect()->back();
    }
    
    public function wip() {
        return view('frontend.wip');
    }
}