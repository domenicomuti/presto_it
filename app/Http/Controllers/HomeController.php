<?php

namespace App\Http\Controllers;

use App\Ad;
use App\AdImage;
use App\Jobs\AdToRevise;
use App\Jobs\ResizeImage;
use Illuminate\Support\Arr;
use Illuminate\Http\Request;
use App\Jobs\TranslateLabels;
use App\Jobs\GoogleDetectFaces;
use App\Jobs\GoogleVisionLabelImage;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use App\Http\Requests\InsertAdValidation;
use App\Jobs\GoogleVisionSafeSearchImage;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware(['auth','verified']);
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function home(){
        $ads = Auth::user()->ads()->where('published',true)->get()->sortByDesc('id');
        return view('home.dashboard',compact('ads'));
    }

    public function delete_ad($id){
        // Cancella prima le immagini, poi il record sul DB
        Storage::deleteDirectory('public/ads/'.$id.'/');
        Ad::find($id)->forceDelete();
        return redirect()->back();
    }

    public function modify_ad($id){
        $temp = "temp_" . now()->format('YmdHisu');
        $ad = Ad::find($id);
        return view('home.modify_ad', compact('ad', 'temp'));
    }

    public function modify_ad_patch(InsertAdValidation $req){
        $temp = $req->temp;
        $id = $req->ad_id;
        $ad = Ad::find($id);

        // MODIFICA INFORMAZIONI
        $ad->title = $req->title;
        $ad->description = $req->description;
        $ad->region_id = $req->region_id;
        $ad->price = $req->price;
        $ad->category_id = $req->category_id;
        $ad->published = null;
        $ad->save();

        // MODIFICA IMMAGINI
        $new_images = session()->get("images.{$id}");
        $new_images = Arr::pluck($new_images, 'file');

        $orig_images = $ad->images()->get('file')->toArray();
        $orig_images = Arr::flatten($orig_images);

        $images_to_remove = array_diff($orig_images, $new_images);
        $images_to_add = array_diff($new_images, $orig_images);

        foreach ($images_to_remove as $image){
            // Cancella da HD
            $ad->images()->get()->where('file',$image)->first()->deleteFile(120,120);
            $ad->images()->get()->where('file',$image)->first()->deleteFile(300,300);
            $ad->images()->get()->where('file',$image)->first()->deleteFile(600,450);
            $ad->images()->get()->where('file',$image)->first()->deleteFile();

            // Cancella da DB
            $ad->images()->where('file', $image)->delete();
        }

        foreach ($images_to_add as $image){
            // Aggiungi su HD
            $dirname = dirname($image);
            $basename = basename($image);
            $dirname_tmp = substr($dirname, 0, strlen($dirname)-25);

            $destPath = $dirname_tmp.$basename;
            Storage::move($image, $destPath);
       
            // Aggiungi su DB
            $adImage = new AdImage(['file' => $destPath]);
            $ad->images()->save($adImage);

            GoogleVisionSafeSearchImage::dispatch($adImage->id);
            GoogleVisionLabelImage::dispatch($adImage->id);
            GoogleDetectFaces::dispatch($adImage->id);
            ResizeImage::dispatch($destPath,120,120);
            ResizeImage::dispatch($destPath,300,300);
            ResizeImage::dispatch($destPath,600,450);
            ResizeImage::dispatch($destPath); // aggiungi watermark a immagine originale
            TranslateLabels::dispatch($adImage->id);
        }

        AdToRevise::dispatch($ad);
        
        Storage::deleteDirectory("public/ads/{$id}/{$temp}");

        return redirect()->route('home.modify_ad.thanks', compact('id'));
    }

    public function modify_ad_thanks(){
        return view('home.modify_thanks');
    }
}