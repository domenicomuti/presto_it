<?php

namespace App\Http\Controllers;

use App\Ad;
use App\Jobs\ResizeImage;
use Illuminate\Support\Arr;
use Illuminate\Http\Request;
use App\Jobs\DeleteTempFolder;
use Illuminate\Support\Facades\Storage;

class DropzoneModifyController extends Controller
{
    public function __construct(){
        $this->middleware(['auth','verified']);
    }

    public function add_img(Request $req, $id){
        /* 
            Il seguente Job (DeleteTempFolder) verrà azionato dopo 5 minuti e servirà a cancellare la cartella con le immagini temporanee.
            In pratica l'utente avrà 5 minuti di tempo per confermare le modifiche all'annuncio.
            Dopo i 5 minuti apparirà a video un alert che lo avviserà che la sessione di modifica è scaduta e verrà reindirizzato verso la dashboard. Tutte le eventuali immagini temporanee caricate verranno automaticamente eliminate.
        */
        $temp = $req->temp;

        DeleteTempFolder::dispatch($id,$temp)->delay(now()->addMinutes(5));

        $filename = $req->file->store("public/ads/{$id}/{$temp}");

        session()->push("images.{$id}", ['file' => $filename, 'orig' => false]);
        return response()->json($filename);
    }

    public function check_img($id){
        /* La funzione andrà a recuperare i files direttamente dall'HD e non dal DB in quanto
        andrà anche a calcolare la dimensione del file (non presente su DB) */

        $exists = Storage::exists("public/ads/{$id}");
        $images = [];
        session()->forget("images.{$id}");
        session()->put("images.orig", 0);

        // La variabile di sessione images.orig conta il numero di files già presenti in origine.
        
        if ($exists){
            $files = Storage::files("public/ads/{$id}");
            $key = 0;

            foreach($files as $file){
                $filename = basename($file);
                if (
                    !preg_match("/(crop120x120_){1}[\s\S]+/", $filename) &&
                    !preg_match("/(crop300x300_){1}[\s\S]+/", $filename) &&
                    !preg_match("/(crop600x450_){1}[\s\S]+/", $filename)
                ){
                    $images[$key]['filename'] = $filename;
                    $images[$key]['size'] = Storage::size("public/ads/{$id}/{$filename}");
                    $key++;
                    session()->put("images.orig", $key);
                    session()->push("images.{$id}", ['file' => $file, 'orig' => true]);
                }
            }
        }
        return response()->json([
            'exists' => $exists,
            'url' => "public/ads/{$id}",
            'storageUrl' => Storage::url("public/ads/{$id}"),
            'images' => $images
        ]);
    }

    public function delete_img(Request $req){
        $filePath = $req->filename;

        $id = $req->id;

        if ($filePath != "undefined"){
            $images = session()->get("images.{$id}");

            foreach ($images as $key => $image){
                if ($image['file'] == $filePath){
                    unset($images[$key]);
                    if ($image['orig']){
                        $orig = session()->get("images.orig");
                        $orig--;
                        session()->put("images.orig", $orig);
                    }
                }
            }
            session()->put("images.{$id}", $images);
        }

        return response()->json([
            'files' => session()->get("images.{$id}"),
            'orig' => session()->get("images.orig")
        ]);
    }
}