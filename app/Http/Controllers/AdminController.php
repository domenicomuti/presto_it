<?php

namespace App\Http\Controllers;

use App\Ad;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class AdminController extends Controller
{
    public function __construct() {
        $this->middleware('auth.admin');
    }

    public function admin() {
        $revisors = User::where('revisor', true)->get();
        $to_be_revisors = User::where('revisor', null)->get();
        return view('admin.dashboard', compact('revisors', 'to_be_revisors'));
    }

    public function add_revisor($id) {
        $user = User::find($id);
        $user->revisor = true;
        $user->save();
        return redirect()->back();
    }

    public function delete_revisor($id) {
        $user = User::find($id);
        $user->revisor = false;
        $user->save();
        return redirect()->back();
    }
}