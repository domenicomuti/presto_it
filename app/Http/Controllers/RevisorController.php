<?php

namespace App\Http\Controllers;

use App\Ad;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class RevisorController extends Controller
{
    public function __construct() {
        $this->middleware('auth.revisor');
    }

    public function revise() {
        $ad = Ad::where('published', false)
            ->orderBy('id', 'asc')
            ->first();
        return view('frontend.ad_details', compact('ad'));
    }

    public function accept(Request $req) {
        $ad = Ad::find($req->id);
        $ad->published = true;
        $ad->category_id = $req->category;
        $ad->save();
        return redirect()->back();
    }

    public function reject(Request $req) {
        $ad = Ad::find($req->id);
        $ad->delete();
        return redirect()->back();
    }

    public function trash(){
        $ads = Ad::onlyTrashed()->paginate(5);
        return view('revisor.trash', compact('ads'));
    }

    public function delete($id){
        Ad::onlyTrashed()->find($id)->forceDelete();
        Storage::deleteDirectory("public/ads/{$id}");
        return redirect()->back();
    }

    public function restore($id){
        Ad::onlyTrashed()->find($id)->restore();
        return redirect()->back();
    } 
}