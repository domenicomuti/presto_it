<?php

namespace App\Http\Controllers;

use App\Jobs\ResizeImage;
use Illuminate\Http\Request;
use App\Jobs\DeleteSecretFolder;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;

class DropzoneController extends Controller
{
    public function __construct(){
        $this->middleware(['auth','verified']);
    }

    public function add_img(Request $req) {
        $secret = $req->secret;
        $filename = $req->file->store("public/temp/{$secret}");

        // Elimina cartella temporanea dopo 5 minuti e invia messaggio di sessione scaduta
        DeleteSecretFolder::dispatch($secret, Auth::id())->delay(now()->addMinutes(5));

        // Anteprima 120x120
        ResizeImage::dispatch($filename,120,120);

        session()->push("images.{$secret}",$filename);
        return response()->json($filename);
    }

    public function check_img(Request $req){
        $secret = $req->secret;
        $exists = Storage::exists("public/temp/{$secret}");
        $images = [];
        if ($exists){
            $files = Storage::files("public/temp/{$secret}");
            foreach($files as $key => $file){
                $filename = basename($file);
                $images[$key]['filename']= $filename;
                $images[$key]['size'] = Storage::size("public/temp/{$secret}/{$filename}");
            }
        }
        return response()->json([
            'exists' => $exists,
            'url' => "public/temp/{$secret}",
            'storageUrl' => Storage::url("public/temp/{$secret}"),
            'images' => $images
        ]);            
    }

    public function delete_img(Request $req){
        $filename = $req->filename;
        $secret = $req->secret;
        session()->push("removedimages.{$secret}",$filename);
        Storage::delete($filename);
        
        // Cancella anche anteprima 120x120
        $basePath = dirname($filename);
        $fileName = basename($filename);
        Storage::delete("{$basePath}/crop120x120_{$fileName}"); 

        // Se non ci sono più files nella cartella, cancellala
        $files = Storage::files("public/temp/{$secret}");
        if (!$files)
            Storage::deleteDirectory("public/temp/{$secret}");

        return response()->json('image deleted');
    }
}