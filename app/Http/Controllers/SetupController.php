<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;

class SetupController extends Controller
{
    public function __construct() {
        $this->middleware(['auth','verified']);
    }

    public function setup() {
        return view('home.setup');
    }

    public function be_revisor($id) {
        $user = User::find($id);
        if (!$user->revisor){
            $user->revisor = null;
            $user->save();
            return redirect()->back()->with('warning', trans('ui.setup.revisor-sent'));
        }
        return redirect()->back()->with('warning', trans('ui.setup.revisor-already'));
    }
}