<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use App\Events\ModifySessionExpired;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Storage;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

class DeleteTempFolder implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $id;
    protected $temp;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($id, $temp)
    {
        $this->id = $id;
        $this->temp = $temp;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $id = $this->id;
        $temp = $this->temp;
        
        if (file_exists(public_path(Storage::url("public/ads/{$id}/{$temp}")))){
            Storage::deleteDirectory("public/ads/{$id}/{$temp}");

            // Avvisa utente che non è più possibile effettuare modifiche
            event(new ModifySessionExpired($id, $temp));
        }
    }
}