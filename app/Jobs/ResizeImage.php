<?php

namespace App\Jobs;

use Spatie\Image\Image;
use Illuminate\Bus\Queueable;
use Spatie\Image\Manipulations;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

class ResizeImage implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    private $path, $fileName, $w, $h;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($filePath, $w = null, $h = null)
    {
        $this->path = dirname($filePath);
        $this->fileName = basename($filePath);
        $this->w = $w;
        $this->h = $h;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $w = $this->w;
        $h = $this->h;
        $srcPath = storage_path() . '/app/' . $this->path . '/' . $this->fileName;

        $img = Image::load($srcPath);
        
        if ($w && $h){
            $img->crop(Manipulations::CROP_CENTER, $w, $h);
            $destPath = storage_path() . '/app/' . $this->path . "/crop{$w}x{$h}_" . $this->fileName;
        }else{
            $destPath = $srcPath;
        }

        $img->watermark(public_path('/img/watermark.png'))
            ->watermarkHeight(40, Manipulations::UNIT_PERCENT)
            ->watermarkWidth(40, Manipulations::UNIT_PERCENT)
            ->watermarkPadding(3, 3, Manipulations::UNIT_PERCENT)
            ->save($destPath);
    }
}