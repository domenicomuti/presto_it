<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use App\Events\AddSessionExpired;
use Illuminate\Support\Facades\Auth;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Storage;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

class DeleteSecretFolder implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $secret;
    protected $id;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($secret, $id)
    {
        $this->secret = $secret;
        $this->id = $id;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $secret = $this->secret;
        $id = $this->id;
        
        if (file_exists(public_path(Storage::url("public/temp/{$secret}")))){
            Storage::deleteDirectory("public/temp/{$secret}");

            // Avvisa utente che non è più possibile effettuare modifiche
            event(new AddSessionExpired($secret, $id));
        }
    }
}