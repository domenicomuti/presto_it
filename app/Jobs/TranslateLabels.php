<?php

namespace App\Jobs;

use App\AdImage;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Google\Cloud\Translate\TranslateClient;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

class TranslateLabels implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    private $ad_image_id;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($ad_image_id)
    {
        $this->ad_image_id = $ad_image_id;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        if (!file_exists(base_path('google_credential.json'))) { return; }
        
        $i = AdImage::find($this->ad_image_id);

        if(!$i) { return; }

        putenv('GOOGLE_APPLICATION_CREDENTIALS=' . base_path('google_credential.json'));

        $results = $i->labels_en;
        $results_it = [];
        $results_es = [];

        foreach ($results as $result){
            $translate = new TranslateClient();
            $translation_it = $translate->translate($result, ['target' => 'it']);
            $translation_es = $translate->translate($result, ['target' => 'es']);
            $results_it[] = $translation_it['text'];
            $results_es[] = $translation_es['text'];
        }

        $i->labels_it = $results_it;
        $i->labels_es = $results_es;
        $i -> save();
    }
}