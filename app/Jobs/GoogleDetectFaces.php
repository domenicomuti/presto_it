<?php

namespace App\Jobs;

use App\AdImage;
use Spatie\Image\Image;
use Illuminate\Bus\Queueable;
use Spatie\Image\Manipulations;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Storage;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Google\Cloud\Vision\V1\ImageAnnotatorClient;

class GoogleDetectFaces implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */

    private $ad_image_id;

    public function __construct($ad_image_id)
    {
        $this->ad_image_id = $ad_image_id;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        if (!file_exists(base_path('google_credential.json'))) { return; }

        $i = AdImage::find($this->ad_image_id);

        if(!$i) { return; }

        putenv('GOOGLE_APPLICATION_CREDENTIALS=' . base_path('google_credential.json'));

        $imageAnnotator = new ImageAnnotatorClient();

        $image = file_get_contents(storage_path('app/' . $i->file));
        $response = $imageAnnotator->faceDetection($image);
        $faces = $response->getFaceAnnotations();

        dump(count($faces)." faces found");
        
        $image = $i->file;

        foreach ($faces as $face) {
            # get bounds
            $vertices = $face->getBoundingPoly()->getVertices();
            $bounds = [];
            foreach ($vertices as $vertex) {
                $bounds[] = [$vertex->getX(), $vertex->getY()];
            }

            Image::load(storage_path()."/app/".$image)
            ->watermark(public_path('/img/bear.png'))
            ->watermarkPosition('top-left')
            ->watermarkPadding($bounds[0][0], $bounds[0][1])
            ->watermarkHeight(($bounds[3][1]-$bounds[0][1]))
            ->watermarkWidth(($bounds[1][0]-$bounds[0][0]))
            ->watermarkFit(Manipulations::FIT_STRETCH)
            ->save(storage_path()."/app/".$image);
        }
    }
}