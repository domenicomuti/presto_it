<?php

use \App\Category;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class PopulateCategories extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        /* CATEGORIE DECISE IN MODO ARBITRARIO */
        $categories = ['Auto','Moto','Elettronica','Informatica','Videogames','Lavoro','Musica','Sport','Animali','Casa'];

        foreach($categories as $category){
            $cat = new Category;
            $cat->name = $category;
            $cat->save();
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        $categories = Category::all();
        foreach($categories as $category){
            $category->delete();
        }
    }
}
