<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddLocalizationResultsToAdImagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('ad_images', function (Blueprint $table) {
            $table->text('labels_it')->nullable()->after('labels');
            $table->text('labels_es')->nullable()->after('labels_it');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('ad_images', function (Blueprint $table) {
            $table->dropColumn('labels_it');
            $table->dropColumn('labels_es');
        });
    }
}
