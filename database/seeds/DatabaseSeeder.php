<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        /* Crea Admin */
        $admin = new App\User;
        $admin->name = 'Admin';
        $admin->email = 'admin@admin.com';
        $admin->email_verified_at = now();
        $admin->password = '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi';
        $admin->admin = true;
        $admin->revisor = true;
        $admin->save();  

        /* Crea 50 utenti e per ogni utente crea da 0 a 30 annunci senza foto (random) */
        factory(App\User::class, 50)->create()->each(function ($user) {
            $user->ads()->saveMany(factory(App\Ad::class, rand(0,30))->make());
        });      
    }
}