<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use \App\Ad;
use \App\Category;
use Faker\Generator as Faker;

$factory->define(Ad::class, function (Faker $faker) {
    $categories = Category::all();
    
    return [       
        'title' => $faker->name,
        'description' => $faker->text(400),
        'region_id' => rand(1,20),
        'price' => rand(1, 10000),
        'category_id' => rand(1, count($categories)),
        'published' => true
    ];
});
