<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/* 
    MIDDLEWARE
    ------------------
    --- check.user ---
    ------------------
    Controlla se l'annuncio appartiene all'utente che fa la request.
    Senza questo controllo qualsiasi utente autenticato potrebbe andare a modificare 
    oppure eliminare annunci che non gli appartengono, semplicemente cambiando ID nell'Url.

    -----------------------
    --- check.published ---
    -----------------------
    Controlla se l'annuncio associato all'ID presente nella request
    è stato approvato per la pubblicazione.

    -----------------------------
    --- AllowUserToFavorites ---
    -----------------------------
    Utilizzato durante l'aggiunta o la rimozione di annunci Preferiti.
    Controlla se nella request è presente l'ID utente dal quale parte la richiesta stessa
    e che l'ID annuncio presente nella request sia effettivamente un annuncio esistente sul DB.
*/

/* Rotte Frontend */
Route::get('/', 'FrontendController@welcome');
Route::get('/ad/{id}/{slug}', 'FrontendController@ad_details')->name('ad_details')
    ->middleware('check.published');
Route::get('/wip', 'FrontendController@wip')->name('wip');

/* Rotte inserimento annuncio (protette di default da Middleware auth) */
Route::get('/insert', 'InsertAdController@insert')->name('insert.page');
Route::post('/insert/add-img', 'DropzoneController@add_img');
Route::get('/insert/check-img', 'DropzoneController@check_img');
Route::delete('/insert/delete-img', 'DropzoneController@delete_img');
Route::post('/insert/posted', 'InsertAdController@insert_post')->name('insert.post');
Route::get('/insert/thanks', 'InsertAdController@insert_thanks')->name('insert.thanks');

/* Rotte Home (protette di default da Middleware auth) */
Route::get('/home', 'HomeController@home')->name('home');
Route::delete('/home/delete-ad/{id}', 'HomeController@delete_ad')->name('home.delete_ad')
    ->middleware('check.user', 'check.published');
Route::get('/home/modify-ad/{id}', 'HomeController@modify_ad')->name('home.modify_ad')
    ->middleware('check.user', 'check.published');
Route::post('/home/modify-ad/{id}/add-img', 'DropzoneModifyController@add_img')
    ->middleware('check.user', 'check.published');
Route::get('/home/modify-ad/{id}/check-img', 'DropzoneModifyController@check_img')
    ->middleware('check.user', 'check.published');
Route::get('/home/modify-ad/{id}/count-img', 'DropzoneModifyController@count_img')
    ->middleware('check.user', 'check.published');
Route::delete('/home/modify-ad/{id}/delete-img', 'DropzoneModifyController@delete_img')
    ->middleware('check.user', 'check.published');
Route::patch('/home/modify-ad/{id}/modified', 'HomeController@modify_ad_patch')->name('home.modify_ad.patch')
    ->middleware('check.user', 'check.published');
Route::get('/home/modify-ad/{id}/thanks', 'HomeController@modify_ad_thanks')->name('home.modify_ad.thanks');

/* Rotte Chat */
Route::get('/home/chat', 'ChatController@chat')->name('chat');

/* Rotte Setup (protette di default da Middleware auth) */
Route::get('/home/setup', 'SetupController@setup')->name('home.setup');
Route::put('/home/setup/be-revisor/{id}', 'SetupController@be_revisor')->name('home.setup.be_revisor');

/* Rotte Revisor (protette di default da Middleware auth.revisor) */
Route::get('/home/revise', 'RevisorController@revise')->name('revise');
Route::get('/home/revise/restore', 'RevisorController@revise_restore')->name('revise.restore');
Route::put('/home/revise/accept', 'RevisorController@accept')->name('revise.accept');
Route::put('/home/revise/reject', 'RevisorController@reject')->name('revise.reject');
Route::get('/home/revise/trash', 'RevisorController@trash')->name('revise.trash');
Route::delete('/home/revise/trash/delete/{id}', 'RevisorController@delete')->name('revise.trash.delete');
Route::put('/home/revise/trash/restore/{id}', 'RevisorController@restore')->name('revise.trash.restore');

/* Rotte Admin (protette di default da Middleware auth.admin */
Route::get('/admin', 'AdminController@admin')->name('admin');
Route::put('/admin/add-revisor/{id}', 'AdminController@add_revisor')->name('admin.add.revisor');
Route::put('/admin/delete-revisor/{id}', 'AdminController@delete_revisor')->name('admin.delete.revisor');

/* Full Text Search */
Route::get('/search', 'SearchController@FullTextSearch')->name('FullTextSearch');

/* Rotte Multilingua */
Route::get('/locale/{locale}', 'FrontendController@locale')->name('locale');

Auth::routes(['verify' => true]); // Commentare il parametro per annullare la verifica di registrazione utente

/* Rotte Registrazione Facebook */
Route::get('/register/facebook', 'Auth\RegisterController@redirectToFacebook')->name('faceboook.register');
Route::get('/register/facebook/callback', 'Auth\RegisterController@handleFacebookCallback');

/* Rotte Gestione Preferiti */
Route::get('/favorite', 'FavoritesController@favorites_home')->name('favorites');
Route::post('/favorite/add', 'FavoritesController@add')->middleware('AllowUserToFavorites');
Route::delete('/favorite/delete', 'FavoritesController@delete')->middleware('AllowUserToFavorites');