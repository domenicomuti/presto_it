<?php

use App\Ad;

/*
|--------------------------------------------------------------------------
| Broadcast Channels
|--------------------------------------------------------------------------
|
| Here you may register all of the event broadcasting channels that your
| application supports. The given channel authorization callbacks are
| used to check if an authenticated user can listen to the channel.
|
*/

Broadcast::channel('ModifySessionExpired.{id}', function ($user, $id) {
    return (int) $user->id === (int) Ad::find($id)->user_id;
});

Broadcast::channel('AddSessionExpired.{id}', function ($user, $id) {
    return (int) $user->id === (int) $id;
});