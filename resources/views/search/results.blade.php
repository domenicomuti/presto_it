@extends('layouts.app')

@section('meta-title')
@if ($query != '')
    <title>{{ ucfirst($query) }}: cerca in tutta Italia</title>
@else
    <title>Presto: cerca in tutta Italia</title>
@endif
@endsection

@section('meta-description')
@if ($query != '')
    <meta name="description" content="{{ ucfirst($query) }} in vendita: scopri subito migliaia di annunci di privati e aziende e trova quello che cerchi su Presto.it">
@else
    <meta name="description" content="Scopri subito migliaia di annunci di privati e aziende e trova quello che cerchi su Presto.it">
@endif
@endsection

@section('content')

@include('includes.searchbar')

<div class="container search-results">
    <div class="row">
        @if($searchResults->count()!=0)
            <section class="col-12 py-4 mb-0 mb-lg-3 header"> {{-- Sezione info --}}
                @if ($query != '')
                    <h1 class="query">{{ __('ui.search.results') }} <span class="number">{{ $query }}</span></h1>
                @else
                    <h1 class="query">{{ __('ui.no-query') }}</h1>
                @endif
                @if ($category != 0)
                    <span class="d-lg-none">{{ __('ui.category') }}: <span class="number">
                        @switch(App::getLocale())
                            @case('it')
                                {{ App\Category::find($category)->name }}
                                @break
                            @case('en')
                                {{ App\Category::find($category)->name_eng }}
                                @break
                            @case('es')
                                {{ App\Category::find($category)->name_esp }}
                                @break
                        @endswitch
                    </span></span><br class="d-lg-none">
                @endif
                @if ($region != 0)
                    <span class="d-lg-none">{{ __('ui.region') }}: <span class="number">{{ App\Region::find($region)->name }}</span></span><br class="d-lg-none">
                @endif
                <span>{{ __('ui.search.total') }} <span class="number">{{ $searchResults->total() }}</span><br>
                    @if($searchResults->total() > $searchResults->perPage())
                        @if($searchResults->firstItem() != $searchResults->lastItem())
                            {{ __('ui.search.ads.start') }} <span class="number">{{ $searchResults->firstItem() }}</span> {{ __('ui.search.ads.to') }} <span class="number">{{ $searchResults->lastItem() }}</span>
                        @else
                            {{ __('ui.search.ad.start') }} <span class="number">{{ $searchResults->firstItem() }}</span>
                        @endif
                            <br>{{ __('ui.search.page') }} {{ $searchResults->currentPage() }}/{{ $searchResults->lastPage() }}
                    @endif
                </span>
            </section>

            {{-- Visualizzazione risultati --}}
            @foreach($searchResults as $ad)
                <div class="col-12 mb-0 mb-lg-3 box">
                    <a href="{{ route('ad_details', ['id'=>$ad->id, 'slug'=>Str::slug($ad->title)]) }}">
                        <div class="row p-3 p-lg-4 inner-box">
                            <aside class="col-4 col-lg-2 p-0">
                                <figure>
                                    <img src="{{ $ad->images->first() ? $ad->images->first()->getStorageUrl(300,300) : '/img/placeholder.png' }}" class="w-100 image" alt="{{ $ad->title }}">
                                </figure>
                            </aside>
                            <div class="col-7 col-lg-10 pr-0 pl-md-4 infos">
                                <section class="upper">
                                    <h2 class="font-weight-bold title">{{ $ad->title }}</h2>
                                    <p class="info">{{ $ad->region->name }} - {{ $ad->created_at->diffForHumans() }}</p>
                                    <p class="price">{{ $ad->price }} €</p>
                                </section>
                                <p class="category d-none d-md-block">{{ $ad->category->name }}</p>
                            </div>
                        </div>
                    </a>
                    @auth
                        <div class="favorites" data-user-id="{{ Auth::id() }}" data-ad-id="{{ $ad->id }}">
                            <button class="btn">
                                @if ($ad->is_favored_by->find(Auth::id()))
                                    <i id="icon-ad-{{ $ad->id }}" class="fas fa-heart icon"></i>
                                @else
                                    <i id="icon-ad-{{ $ad->id }}" class="far fa-heart icon"></i>
                                @endif
                            </button>
                        </div>
                    @endauth
                </div>
            @endforeach

            <div class="col-12 p-0">
                {{ $searchResults->appends(Request::all())->onEachSide(1)->links() }}
            </div>
        @else
            <div class="col-12 text-center text-lg-left">
                <p class="py-4 py-lg-0">{{ __('ui.search.no') }}</p>
            </div>
        @endif

    </div>
</div>

@endsection