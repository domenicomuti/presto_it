@extends('layouts.app')

@section('meta-title')
<title>{{ config('app.name', 'Laravel') }}: admin</title>
@endsection

@section('content')

<div class="container admin-dashboard">

    @if (\App\User::count_to_be_revisor()!=0)
        <h3 class="pt-4 text-center">{{ __('ui.admin.add.revisor') }}</h3>
        <div class="row justify-content-center">
            <div class="col-12 col-lg-8 p-0">
                <div class="table">
                    <div class="table-row head">
                        <div class="table-col">{{ __('ui.name') }}</div>
                        <div class="table-col">Email</div>
                        <div class="table-col">{{ __('ui.admin.revisor.add') }}</div>
                    </div>  
                    @foreach($to_be_revisors as $revisor)        
                        <div class="table-row">
                            <div class="table-col">{{ $revisor->name }}</div>
                            <div class="table-col">{{ $revisor->email }}</div>
                            <div class="table-col">
                                <form action="{{ route('admin.add.revisor', $revisor->id) }}" method="POST">
                                    @csrf
                                    @method('PUT')
                                    <button type="submit" class="btn">{{ __('ui.admin.revisor.add') }}</button>
                                </form>
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>
        </div>
    @endif

    @if (\App\User::count_revisor()!=0)
        <h3 class="pt-4 text-center">{{ __('ui.admin.revisor-list') }}</h3>
        <div class="row justify-content-center">
            <div class="col-12 col-lg-8 p-0">
                <div class="table">
                    <div class="table-row head">
                        <div class="table-col">{{ __('ui.name') }}</div>
                        <div class="table-col">Email</div>
                        <div class="table-col">{{ __('ui.admin.revisor.remove') }}</div>
                    </div>  
                    @foreach($revisors as $revisor)
                        <div class="table-row">
                            <div class="table-col">{{ $revisor->name }}</div>
                            <div class="table-col">{{ $revisor->email }}</div>
                            <div class="table-col">
                                <form action="{{ route('admin.delete.revisor', $revisor->id) }}" method="POST">
                                    @csrf
                                    @method('PUT')
                                    <button type="submit" class="btn">{{ __('ui.admin.revisor.remove') }}</button>
                                </form>
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>
        </div>
    @else
        <h3 class="pt-4 text-center">{{ __('ui.admin.no-revisor') }}</h3>
    @endif

</div>

@endsection