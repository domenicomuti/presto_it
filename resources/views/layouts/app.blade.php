<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    @yield('meta-description')

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    @yield('meta-title')

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>
    @stack('scripts')

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">

    <!-- Styles -->
    
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    @stack('styles')
    
</head>

<body class="preload">
    <div id="app" class="container">
        <div class="row justify-content-center">
            <div class="col-12 col-lg-11 px-0">

                <header>
                    @include('includes.nav')
                </header>

                @include('includes.sidebar')

                <main class="page">
                    @yield('content')
                </main>

                @include('includes.footbar')
                
            </div>
        </div>
    </div>
    <script>
    document.addEventListener("DOMContentLoaded", () => {
        document.querySelector('body').classList.remove('preload');
    });
    </script>
</body>
</html>