<nav id="sidebar">
    <div class="container">

        <div class="button">
            <i id="sidebar-close" class="fas fa-times fa-2x" style="z-index: inherit;"></i>
        </div>

        <div id="sidebar-content">

            {{-- Admin --}}
            @if (Auth::user() && Auth::user()->admin)
                <a href="{{ route('admin') }}">
                    <i class="fas fa-user-cog pr-2"></i>
                    <span class="bg-white-dom">Admin</span>
                </a>
            @endif

            {{-- Revisor --}}
            @if(Auth::user() && Auth::user()->revisor)
                <a href="{{ route('revise') }}">
                    <i class="fas fa-wrench"></i>
                    <span class="bg-white-dom">{{ __('ui.review') }}</span>
                    <span class="counter">{{ App\Ad::toBeRevisioned() }}</span>
                </a>
                <a href="{{ route('revise.trash') }}">
                    <i class="fas fa-trash-restore pr-2"></i>
                    <span class="bg-white-dom">{{ __('ui.trash') }}</span>
                </a>
            @endif

            {{-- Dashboard --}}
            <a href="{{ route('home') }}">
                <i class="fas fa-pager"></i>
                <span class="bg-white-dom">{{ __('ui.ads.mine') }}</span>
            </a>

            {{-- Favorites --}}
            <a href="{{ route('favorites') }}">
                <i class="far fa-heart"></i>
                <span class="bg-white-dom">{{ __('ui.favorites') }}</span>
            </a>

            {{-- Setup --}}
            @auth
                <a class="username" href="{{ route('home.setup') }}">
                    <i class="fas fa-cog pr-3"></i>
                    <span class="bg-white-dom">{{ Auth::user()->name }}</span>
                </a>
            @endauth
        </div>

        {{-- Login / Register  --}}
        @guest
            <div id="sidebar-footer">
                <a class="btn text-white btn-block btn-blue" href="{{ route('login') }}" role="button">{{ __('Login') }}</a>
                <p class="m-0">{{ __('ui.no.account') }}</p>
                <strong><a href="{{ route('register') }}">{{ __('ui.register') }}</a></strong>   
            </div>
        @endguest

        {{-- Set Locale --}}
        <div class="sidebar-lang">
            <a href = "{{ route('locale', 'it') }}"><span class="flag-icon flag-icon-it {{ App::isLocale('it') ? 'active' : '' }}"></span></a>
            <a href = "{{ route('locale', 'en') }}"><span class="flag-icon flag-icon-gb {{ App::isLocale('en') ? 'active' : '' }}"></span></a>
            <a href = "{{ route('locale', 'es') }}"><span class="flag-icon flag-icon-es {{ App::isLocale('es') ? 'active' : '' }}"></span></a>
        </div>
    </div>
</nav>

<div id="bg-layer"></div>