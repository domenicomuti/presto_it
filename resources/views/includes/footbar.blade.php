<section id="footbar" class="container-fluid">

    <div class="row bg-yellow">
        <div class="col-12">
            <div class="text-right">
                <i id="footbar-close" class="fas fa-times fa-2x"></i>
            </div>
        </div>
        <div class="mb-4 pl-4">
            @include('includes.logo')
        </div>
    </div>

    <div class="row justify-content-center content">
        <div class="col-12 col-md-6">

            <form action="{{ route('FullTextSearch') }}" method="GET">

                <div class="top mt-4">
                    
                    <h6 class="mb-1"><strong>{{ __('ui.query') }}</strong></h6>
    
                    <div class="input-group mb-2">
                        <div class="input-group-prepend">
                            <span class="input-group-text"><i class="fas fa-search"></i></span>
                        </div>
                        <input name="query" class="form-control input-inner right border-0" type="text" placeholder="{{ __('ui.query.placeholder') }}" value="{{ isset($query) ? $query : '' }}">
                    </div>
    
                    <div class="input-group">
                        <div class="input-group-prepend">
                            <span class="input-group-text"><i class="fas fa-th-large"></i></span>
                        </div>
                        <select name="category" class="form-control input-inner right border-0">
                            <option value=0>{{ __('ui.query.category.placeholder') }}</option>
                            @foreach ($categories as $category_search)
                                @switch(App::getLocale())
                                    @case('it')
                                        <option value="{{ $category_search->id }}" @if (isset($category)) {{ $category == $category_search->id ? "selected" : '' }} @endif>{{ $category_search->name }}</option>
                                        @break
                                    @case('en')
                                        <option value="{{ $category_search->id }}" @if (isset($category)) {{ $category == $category_search->id ? "selected" : '' }} @endif>{{ $category_search->name_eng }}</option>
                                        @break
                                    @case('es')
                                        <option value="{{ $category_search->id }}" @if (isset($category)) {{ $category == $category_search->id ? "selected" : '' }} @endif>{{ $category_search->name_esp }}</option>
                                        @break
                                @endswitch
                            @endforeach
                        </select>
                    </div>
    
                </div>
                
                <div class="bottom mt-4">
                    <h6 class="mb-1"><strong>{{ __('ui.query.position') }}</strong></h6>
    
                    <div class="input-group">
                        <div class="input-group-prepend">
                            <span class="input-group-text"><i class="fas fa-map-marker-alt"></i></span>
                        </div>
                        <select name="region" class="form-control input-inner right border-0">
                            <option value=0>{{ __('ui.query.position.placeholder') }}</option>
                            @foreach($regions as $region_search)
                                <option value="{{ $region_search->id }}" @if (isset($region)) {{ $region == $region_search->id ? "selected" : '' }} @endif>{{ $region_search->name }}</option>
                            @endforeach
                        </select>
                    </div>
    
                </div>
    
                <button type="submit" class="btn btn-block mt-5 mb-4 btn-yellow laptop"><strong>{{ __('ui.search.button') }}</strong></button> 

            </form>
            
        </div>
    </div>

</section>