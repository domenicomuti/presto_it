<a role="button" href="{{ route('insert.page') }}" class="btn btn-yellow px-3">
    <i class="far fa-plus-square pr-2"></i> 
    <span class="font-weight-bold">{{ __('ui.ad.insert') }}</span>
</a>