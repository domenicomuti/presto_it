<section class="container d-none d-lg-block">
    <div class="row">
        <div class="col-12 px-0">
            <form action="{{ route('FullTextSearch') }}" method="GET">
                <div class="searchbar d-flex align-items-end my-5 p-4 shadow">
                    
                    <div class="pr-3">
                        <h6><strong>{{ __('ui.query') }}</strong></h6>
                        <div class="input-group">
                            <div class="input-group-prepend">
                                <span class="input-group-text"><i class="fas fa-search"></i></span>
                            </div>
                            <input name="query" class="form-control input-inner border-0" type="text" placeholder="{{ __('ui.query.placeholder') }}" value="{{ isset($query) ? $query : '' }}">
                        </div>
                    </div>
                    
                    <div class="pr-3">
                        <h6><strong>{{ __('ui.query.category') }}</strong></h6>
                        <div class="input-group pointer">
                            <div class="input-group-prepend">
                                <span class="input-group-text"><i class="fas fa-th-large"></i></span>
                            </div>
                            <select name="category" class="form-control input-inner border-0">
                                <option value=0>{{ __('ui.query.category.placeholder') }}</option>
                                @foreach ($categories as $category_search)
                                    @switch(App::getLocale())
                                        @case('it')
                                            <option value="{{ $category_search->id }}" @if (isset($category)) {{ $category == $category_search->id ? "selected" : '' }} @endif>{{ $category_search->name }}</option>
                                            @break
                                        @case('en')
                                            <option value="{{ $category_search->id }}" @if (isset($category)) {{ $category == $category_search->id ? "selected" : '' }} @endif>{{ $category_search->name_eng }}</option>
                                            @break
                                        @case('es')
                                            <option value="{{ $category_search->id }}" @if (isset($category)) {{ $category == $category_search->id ? "selected" : '' }} @endif>{{ $category_search->name_esp }}</option>
                                            @break
                                    @endswitch
                                @endforeach
                            </select>
                        </div>
                    </div>
                    
                    <div class="pr-3">
                        <h6><strong>{{ __('ui.query.position') }}</strong></h6>
                        <div class="input-group pointer">
                            <div class="input-group-prepend">
                                <span class="input-group-text"><i class="fas fa-map-marker-alt"></i></span>
                            </div>
                            <select name="region" class="form-control input-inner border-0">
                                <option value=0>{{ __('ui.query.position.placeholder') }}</option>
                                @foreach($regions as $region_search)
                                    <option value="{{ $region_search->id }}" @if (isset($region)) {{ $region == $region_search->id ? "selected" : '' }} @endif>{{ $region_search->name }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    
                    <button type="submit" class="btn btn-sm btn-yellow"><i class="fas fa-search"></i></button>
                </div>
            </form>
        </div>
    </div>
</section>