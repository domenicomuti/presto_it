<nav class="navbar navbar-expand-lg navbar-light p-0 py-lg-3 border-bottom">
    <a class="navbar-brand d-none d-lg-block" href="{{ url('/') }}">
        @include('includes.logo')
    </a>

    {{-- Smartphone / Tablet --}}

    <div class="smartphone d-lg-none">

        {{-- Home --}}
        <a class="btn p-1 {{ (url()->current() == url('/')) ? 'active' : '' }} bg-white-dom" role="button" href="{{ url('/') }}">
            <i class="fas fa-forward"></i>
            <span class="mb-0 font-weight-bold">Home</span>
        </a>

        {{-- Search --}}
        <button type="button" class="btn p-1 bg-white-dom footbar-open {{ (url()->current() == route('FullTextSearch')) ? 'active' : '' }}">
            <i class="fas fa-search"></i>
            <span class="mb-0 font-weight-bold">{{ __('ui.search.button') }}</span>
        </button>

        {{-- Insert Ad --}}
        <a class="btn p-1 {{ (url()->current() == route('insert.page')) ? 'active' : '' }} bg-white-dom" role="button" href="{{route('insert.page')}}">
            <i class="far fa-plus-square"></i>
            <span class="mb-0 font-weight-bold">{{ __('ui.insert') }}</span>
        </a>

        {{-- Messages --}}
        <a class="btn p-1 {{ (url()->current() == route('chat')) ? 'active' : '' }} bg-white-dom" role="button" href="{{ route('chat') }}">
            <i class="far fa-comment-dots"></i>
            <span class="mb-0 font-weight-bold">{{ __('Messaggi') }}</span>
        </a>

        {{-- Sidebar Open --}}
        <button id="sidebar-open" class="btn p-1 {{ (url()->current() == route('login') or url()->current() == route('register')) ? 'active' : '' }} bg-white-dom" type="button">
            @guest
                <i class="fas fa-user"></i>
                <span class="mb-0 font-weight-bold">{{ __('ui.login') }}</span>
            @else
                <i class="far fa-user-circle"></i>
                <span class="mb-0 font-weight-bold">{{ Auth::user()->name }}</span>
            @endguest
        </button>
    </div>

    <!-- Laptop -->
    <div class="laptop ml-auto d-none d-lg-block">
        <div class="navbar-nav">
            <!-- Authentication Links -->
            @guest
                <div class="nav-item">
                    <a class="nav-link" href="{{ route('login') }}"><span class="font-weight-bold mr-3">{{ __('Login') }}</span></a>
                </div>
                <div class="nav-item">
                    <a class="nav-link" href="{{ route('register') }}"><span class="mr-4">{{ __('ui.register') }}</span></a>
                </div>
            @else

            {{-- Usermenu --}}
            <div class="usermenu">

                <div class="usermenu-btn d-flex align-items-center px-3">
                    <i class="far fa-user-circle ico"></i>
                    <span style="margin-left: 3px;">{{ Auth::user()->name }}</span>
                    <i class="fas fa-angle-down"></i>
                </div>
                
                <div class="dropmenu">

                    {{-- Dashboard --}}
                    <div>
                        <a href="{{ route('home') }}" class="px-3">
                            <i class="fas fa-pager ico"></i>
                            <span>{{ __('ui.ads.mine') }}</span>
                        </a>
                    </div>

                    {{-- Messages --}}
                    <div>
                        <a href="{{ route('chat') }}" class="px-3">
                            <i class="far fa-comment-dots ico"></i>
                            <span>{{ __('Messaggi') }}</span>
                        </a>
                    </div>

                    {{-- Favorites --}}
                    <div>
                        <a href="{{ route('favorites') }}" class="px-3">
                            <i class="far fa-heart ico"></i>
                            <span>{{ __('ui.favorites') }}</span>
                        </a>
                    </div>

                    {{-- Setup --}}
                    <div>
                        <a href="{{ route('home.setup') }}" class="px-3">
                            <i class="fas fa-cog ico"></i>
                            <span>{{ __('ui.setup') }}</span>
                        </a>
                    </div>

                    {{-- Logout --}}
                    <div>
                        <a href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();" class="px-3">
                            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                @csrf
                            </form>
                            <i class="fas fa-power-off ico"></i>
                            <span>{{ __('Logout') }}</span>
                        </a>
                    </div>

                </div>                  
            </div>
            @endguest

            {{-- Insert Ad --}}
            <div class="nav-item">
                @include('includes.insert_ad_button')
            </div>

            {{-- Revisor --}}
            @if (Auth::user() && Auth::user()->revisor)
                <div class="nav-item revisor-admin-btn">
                    <a role="button" href="{{ route('revise') }}" class="btn btn-blue px-3">
                        <i class="fas fa-wrench pr-2"></i>
                        <span>{{ __('ui.review') }}</span>
                        <div class="counter ml-2 rounded">{{ App\Ad::toBeRevisioned() }}</div>
                    </a>
                </div>
                <div class="nav-item revisor-admin-btn">
                    <a role="button" href="{{ route('revise.trash') }}" class="btn btn-blue px-3">
                        <i class="fas fa-trash-restore"></i>
                    </a>
                </div>
            @endif

            {{-- Admin --}}
            @if (Auth::user() && Auth::user()->admin)
                <div class="nav-item revisor-admin-btn">
                    <a role="button" href="{{ route('admin') }}" class="btn btn-blue px-3">
                        <i class="fas fa-user-cog pr-2"></i>
                        <span>Admin</span>
                    </a>
                </div>
            @endif

            {{-- Set Locale --}}
            <div class="set-locale">
                @foreach(['it','en','es'] as $mainLang)
                    @if(App::isLocale($mainLang))
                        <span class="flag-icon flag-icon-{{ $mainLang=='en' ? 'gb' : $mainLang }}"></span>
                        <div class="dropdown">
                            @foreach(['it','en','es'] as $link)
                                @if(!App::isLocale($link))
                                    <a href="{{ route('locale', $link) }}"><span class="flag-icon flag-icon-{{ $link=='en' ? 'gb' : $link }}"></span></a>
                                @endif
                            @endforeach
                        </div>
                    @endif
                @endforeach
            </div>
            
        </div>
    </div>
</nav>