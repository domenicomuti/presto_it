@extends('layouts.app')

@section('meta-title')
<title>{{ config('app.name', 'Laravel') }}: {{ __('ui.ad.insert.thanks') }}</title>
@endsection

@section('content')

<div class="container">
    <div class="row text-center pt-4 justify-content-center">
        <div class="col-12 col-md-6">
            <p><strong>{{ __('ui.ad.insert.thanks') }}</strong></p>
            <p class="mb-4">{{ __('ui.ad.insert.modify.sub') }}</p>

            <div class="row">
                <div class="col-12 col-md-6 mb-4">
                    <a href="{{ route('insert.page') }}" class="btn btn-lg btn-block btn-blue px-0" tabindex="-1" role="button" aria-disabled="true">{{ __('ui.ad.insert.modify.new') }}</a>
                </div>
                <div class="col-12 col-md-6">
                    <a href="/" class="btn btn-lg btn-block btn-yellow px-0" tabindex="-1" role="button" aria-disabled="true">{{ __('ui.ad.insert.modify.back') }}</a>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection