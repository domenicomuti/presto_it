@extends('layouts.app')

@section('meta-title')
<title>{{ config('app.name', 'Laravel') }}: trash</title>
@endsection

@section('content')
<div class="container trash">
    <div class="row">
        @if($ads->count()!=0)
            @foreach ($ads as $ad)
                <div class="col-12 my-3">
                    <div class="card">
                        <div class="card-header">{{ __('ui.trash.ad') }} #{{ $ad->id }}
                        </div>
                        <div class="card-body">
                            <h5 class="card-title">{{ $ad->title }}</h5>
                            <p class="card-text">{{ $ad->description }}</p>
                            <div class="row">
                                @foreach($ad->images as $image)
                                    <div class="col-6 col-lg-3 mb-3">
                                        <img src="{{ $image->getStorageUrl(300,300) }}" class="w-100" alt="">
                                    </div>
                                @endforeach
                            </div>
                        </div>
                        <div class="card-footer px-1 text-center">
                            <button form="undo" type="submit" class="btn btn-success" formaction="{{ route('revise.trash.restore', $ad->id) }}" formmethod="POST" >{{ __('ui.trash.restore') }}</button>
    
                            <button form="delete" type="submit" class="btn btn-danger" formaction="{{ route('revise.trash.delete', $ad->id) }}" formmethod="POST">{{ __('ui.trash.delete') }}</button>
                        </div>
                    </div>
                </div>
            @endforeach
            <div class="col-12">
                {{ $ads->links() }}
            </div>
        @else
            <div class="col-12 text-center">
                <p class="pt-4 ">{{ __('ui.trash.no-ads') }}</p>
                <a href="{{ route('revise') }}">{{ __('ui.trash.back.action') }}</a>
            </div>
        @endif
    </div>
</div>

<form id="undo" hidden>
    @csrf
    @method("PUT")
</form>

<form id="delete" hidden>
    @csrf
    @method("DELETE")
</form>

@endsection