@extends('layouts.app')

@section('meta-title')
<title>Presto: {{ strtolower(__('ui.auth.verify-title')) }}</title>
@endsection

@section('content')

<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8 my-4">
            <h1 class="mb-4 auth-title">{{ __('ui.auth.verify-title') }}</h1>
            <div class="card">
                <div class="card-header">{{ __('ui.auth.verify-title') }}</div>

                <div class="card-body">
                    @if (session('resent'))
                        <div class="alert alert-success" role="alert">
                            {{ __('ui.auth.verify-1') }}
                        </div>
                    @endif

                    {{ __('ui.auth.verify-2') }}<br>
                    {{ __('ui.auth.verify-3') }},
                    <button form="confirm-action" type="submit" class="btn btn-link p-0 m-0 align-baseline">
                        {{ __('ui.auth.verify-link') }}
                    </button>.

                    <form id="confirm-action" class="d-inline" method="POST" action="{{ route('verification.resend') }}">
                        @csrf
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
