@extends('layouts.app')

@section('meta-title')
<title>{{ config('app.name', 'Laravel') }}: {{ __('ui.welcome.title') }}</title>
@endsection

@section('meta-description')
<meta name="description" content="{{ __('ui.welcome.description') }}">
@endsection

@section('content')

{{-- Smartphone / Tablet --}}
<div class="container d-lg-none">
    <div class="row justify-content-center">
        <div class="col-11 mt-4 text-center">
            @include('includes.logo')
        </div>
        <div class="col-11 mt-4">
            <button type="button" class="footbar-open fake-search-btn btn btn-block btn-lg shadow bg-white-dom">
                <i class="fas fa-search"></i>
                <span>{{ __('ui.query') }}</span>
                <i class="fas fa-chevron-right"></i>
            </button>
        </div>
    </div>
</div>

{{-- Laptop --}}

{{-- Searchbar --}}
@include('includes.searchbar')
            
{{-- Ultimi 8 annunci inseriti --}}
<div class="container mt-5 mt-lg-2 last-ads px-4 px-lg-0">
    <h1 class="mb-4 text-center last"><strong>{{ __('ui.last') }}</strong></h1>
    <div class="row justify-content-center">

        @foreach($ads as $ad)
            <section class="col-6 col-md-4 col-lg-3 mb-2 mb-lg-4 px-1 px-lg-3">
                <div class="ad-window">
                    {{-- Card / Link Cliccabile --}}
                    <a href="{{ route('ad_details', ['id'=>$ad->id,'slug'=>Str::slug($ad->title)]) }}" class="ad-link">
                        <div class="card index-card">
                            <div class="card-img-top">
                                <figure>
                                    <img src="{{ $ad->images()->first() ? $ad->images()->first()->getStorageUrl(300,300) : '/img/placeholder.png' }}" alt="{{ "{$ad->category->name} {$ad->title}" }}" class="w-100">
                                </figure>
                                <div class="price position-absolute rounded">€ {{ $ad->price }}</div>
                            </div>
                            <div class="card-body">
                                <h2 class="card-title">{{ $ad->title }}</h2>
                                <p class="card-text">{{ $ad->region->name }}<span class="date">, {{ $ad->created_at->diffForHumans() }}</span></p>
                            </div>
                        </div>
                    </a>

                    {{-- Sotto-menu visibile solo su Laptop, comandi di Contatta / Metti nei Preferiti --}}
                    <div class="commands d-none d-lg-block">

                        {{-- Pulsante Contatta Venditore --}}
                        <div class="action">
                            <button class="btn">
                                <i class="far fa-comment-dots icon"></i>
                                <span>{{ __('ui.message.contact') }}</span>
                            </button>
                        </div>

                        {{-- Pulsante Metti nei Preferiti --}}
                        @auth
                            <div class="action favorites" data-user-id="{{ Auth::id() }}" data-ad-id="{{ $ad->id }}">
                                <button class="btn">
                                    @if ($ad->is_favored_by->find(Auth::id()))
                                        <i id="icon-ad-{{ $ad->id }}" class="fas fa-heart icon"></i>
                                        <span id="msg-fav-{{ $ad->id }}">{{ __('Togli dai Preferiti') }}</span>
                                    @else
                                        <i id="icon-ad-{{ $ad->id }}" class="far fa-heart icon"></i>
                                        <span id="msg-fav-{{ $ad->id }}">{{ __('Metti tra i Preferiti') }}</span>
                                    @endif
                                </button>
                            </div>
                        @endauth
                    </div>
                </div>
            </section>
        @endforeach
    </div>
</div>

@endsection