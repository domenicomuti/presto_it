@extends('layouts.app')

@section('meta-title')
@if (isset($ad))
    <title>{{ $ad->title }}</title>
@else
    <title>Presto: Revisor Mode</title>
@endif
@endsection

@section('meta-description')
@if (isset($ad))
    <meta name="description" content="{{ substr($ad->description,0,160) }}">
@endif
@endsection

@section('content')

@if ($ad)
    <div class="container ad-details">

        <nav class="row"> {{-- Sezione Breadcrumb --}}
            <div class="col-12 px-0 py-lg-2 px-4">
                <div class="d-none d-md-block">
                    <div aria-label="breadcrumb">
                        <ol class="breadcrumb m-0 px-0">
                            <li class="breadcrumb-item"><a href="/">Home</a></li>
                            <li class="breadcrumb-item">
                                <button type="submit" class="btn btn-link" form="form-region">{{ $ad->region->name }}</button>
                            </li>
                            <li class="breadcrumb-item">
                                <button type="submit" class="btn btn-link" form="form-category">
                                    @switch(App::getLocale())
                                        @case('it') {{ $ad->category->name }} @break
                                        @case('en') {{ $ad->category->name_eng }} @break
                                        @case('es') {{ $ad->category->name_esp }} @break
                                    @endswitch
                                </button>
                            </li>
                        </ol>
                    </div>
                </div>
                <form id="form-region" action="{{ route('FullTextSearch') }}" method="GET">
                    <input name="region" value="{{ $ad->region_id }}" hidden> 
                </form>
                <form id="form-category" action="{{ route('FullTextSearch') }}" method="GET">
                    <input name="category" value="{{ $ad->category_id }}" hidden>
                </form>
            </div>
        </nav>

        <article class="row">
            <aside class="col-12 col-lg-8 px-0 pl-lg-4">
                @if (count($ad->images)!=0)
                    <div class="slick-carousel">
                        @foreach($ad->images as $image)
                            {{-- REVISOR MODE TABLET --}}
                            @if(Auth::user() && Auth::user()->revisor && !$ad->published)
                                <div class="pic m-0">
                                    <div class="img-revisor">
                                        <img src="{{ $image->getStorageUrl(600,450) }}" alt="{{ $ad->title }}" class="w-100">
                                    </div>
                                    <div class="px-3 pb-2 capt bottom">
                                        <span>Tags: </span>
                                        @if ($image->labels_en)
                                            @switch (App::getLocale())
                                                @case('it')
                                                    @foreach ($image->labels_it as $label)
                                                        @if (!$loop->last)
                                                            <span>{{ $label }}, </span>  
                                                        @else
                                                            <span>{{ $label }}.</span>
                                                        @endif
                                                    @endforeach
                                                    @break
                                                @case('en')
                                                    @foreach ($image->labels_en as $label)
                                                        @if (!$loop->last)
                                                            <span>{{ $label }}, </span>  
                                                        @else
                                                            <span>{{ $label }}.</span>
                                                        @endif
                                                    @endforeach
                                                    @break
                                                @case('es')
                                                    @foreach ($image->labels_es as $label)
                                                        @if (!$loop->last)
                                                            <span>{{ $label }}, </span>  
                                                        @else
                                                            <span>{{ $label }}.</span>
                                                        @endif
                                                    @endforeach
                                                    @break
                                            @endswitch
                                        @else
                                            <span>{{ __('ui.google.warning2') }}</span>
                                        @endif
                                    </div>
                                    <div class="capt top p-2">
                                        <div class="bar">
                                            @include('includes.progress_bar', ['type'=>$image->adult, 'name'=>'adult'])
                                        </div>
                                        <div class="bar">
                                            @include('includes.progress_bar', ['type'=>$image->spoof, 'name'=>'spoof'])
                                        </div>
                                        <div class="bar">
                                            @include('includes.progress_bar', ['type'=>$image->medical, 'name'=>'medical'])
                                        </div>
                                        <div class="bar">
                                            @include('includes.progress_bar', ['type'=>$image->violence, 'name'=>'violence'])
                                        </div>
                                        <div class="bar">
                                            @include('includes.progress_bar', ['type'=>$image->racy, 'name'=>'racy'])
                                        </div>
                                        @if (!$image->adult)
                                            <span class="label warning">{{ __('ui.google.warning') }}</span>
                                        @endif
                                    </div>
                                </div>
                            @else {{-- USER MODE --}}
                                <figure>
                                    <img src="{{ $image->getStorageUrl(600,450) }}" alt="{{ $ad->title }}" class="w-100">
                                </figure>
                            @endif
                        @endforeach
                    </div>
                    {{-- REVISOR MODE --}}
                    @if (!$ad->published && $ad->images->first()->adult)
                        <div class="slick-bottom d-md-none">
                            @foreach ($ad->images as $image)
                                <div>
                                    <div class="bar">
                                        @include('includes.progress_bar', ['type'=>$image->adult, 'name'=>'adult'])
                                    </div>
                                    <div class="bar">
                                        @include('includes.progress_bar', ['type'=>$image->spoof, 'name'=>'spoof'])
                                    </div>
                                    <div class="bar">
                                        @include('includes.progress_bar', ['type'=>$image->medical, 'name'=>'medical'])
                                    </div>
                                    <div class="bar">
                                        @include('includes.progress_bar', ['type'=>$image->violence, 'name'=>'violence'])
                                    </div>
                                    <div class="bar">
                                        @include('includes.progress_bar', ['type'=>$image->racy, 'name'=>'racy'])
                                    </div>
                                </div>
                            @endforeach
                        </div>
                    @endif
                @else
                    <figure>
                        <img src="/img/placeholder_details.png" alt="presto logo" class="w-100 d-none d-lg-block">
                    </figure>
                @endif
            </aside>
        
            <section class="col-12 col-lg-4"> {{-- Sezione informazioni --}}
                <div class="details-col">
                    <div class="row px-4 pl-lg-4">
                        <div class="col-12 px-0 py-3 category">

                            {{-- REVISOR MODE --}}
                            @if(Auth::user() && Auth::user()->revisor && !$ad->published)
                                <form id="mainForm">
                                    @csrf
                                    @method('PUT')
                                    <input name="id" value="{{ $ad->id }}" hidden>
                                    <div class="input-group pointer">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text"><i class="fas fa-th-large"></i></span>
                                        </div>
                                        <select name="category" class="form-control input-inner border-0">
                                            @foreach ($categories as $category_app)
                                                <option value="{{ $category_app->id }}" {{ $category_app->id == $ad->category_id ? 'selected': '' }}>
                                                    @switch(App::getLocale())
                                                        @case('it') {{ $category_app->name }} @break
                                                        @case('en') {{ $category_app->name_eng }} @break
                                                        @case('es') {{ $category_app->name_esp }} @break
                                                    @endswitch
                                                </option>
                                            @endforeach
                                        </select>
                                    </div>
                                </form>
                            @else {{-- USER MODE --}}
                                <span class="name">
                                    @switch(App::getLocale())
                                        @case('it') {{ $ad->category->name }} @break
                                        @case('en') {{ $ad->category->name_eng }} @break
                                        @case('es') {{ $ad->category->name_esp }} @break
                                    @endswitch
                                </span>
                                @auth
                                    <div class="favorites" data-user-id="{{ Auth::id() }}" data-ad-id="{{ $ad->id }}">
                                        <button class="btn">
                                            @if ($ad->is_favored_by->find(Auth::id()))
                                                <i id="icon-ad-{{ $ad->id }}" class="fas fa-heart icon"></i>
                                            @else
                                                <i id="icon-ad-{{ $ad->id }}" class="far fa-heart icon"></i>
                                            @endif
                                        </button>
                                    </div>
                                @endauth
                            @endif
                        </div>
                        <div class="col-12 px-0 py-3 date">
                            <span>{{ $ad->created_at->diffForHumans() }}</span>
                            <span>ID: {{ $ad->id }}</span>
                        </div>
                        <div class="col-12 px-0 pb-3">
                            <h1 class="title">{{ $ad->title }}</h1>
                        </div>
                        <div class="col-12 px-0 pb-3 region">
                            <i class="fas fa-map-marker-alt"></i>
                            <span>{{ $ad->region->name }}</span>
                        </div>
                        <div class="col-12 px-0 pb-3 price">
                            <span>{{ $ad->price }} €</span>
                        </div>
                    </div>
                    <div class="row px-4 pl-lg-4">
                        <div class="col-12 px-0 py-3 user">
                            <span class="first-letter">{{ substr($ad->user->name,0,1) }}</span>
                            <span class="name">{{ $ad->user->name }}</span>
                        </div>
                    </div>
                </div>
            </section>

            <div class="container">

                <div class="row">
                    <div class="col-12 px-4">
                        <hr class="mb-4">
                    </div>
                </div>

                <section> {{-- Sezione descrizione --}}
                    <div class="row description">
                        <div class="col-12 col-lg-8 pl-4 pr-4 pr-lg-0">
                            <h2 class="font-weight-bold mb-4">{{ __('ui.ad.details.description') }}</h2>
                            <p class="mb-4">{{ $ad->description }}</p>
                            <hr>
                        </div>
                    </div>
                </section>

                {{-- REVISOR MODE --}}
                @if(Auth::user() && Auth::user()->revisor && !$ad->published)
                    <div class="container mb-4">
                        <div class="row">
                            <div class="col-6 col-lg-4">
                                <button form="mainForm" formaction="{{ route('revise.accept') }}" formmethod="POST" type="submit" class="btn btn-success btn-block">{{ __('ui.ad.details.revise.accept') }}</button>
                            </div>
                            <div class="col-6 col-lg-4">
                                <button form="mainForm" formaction="{{ route('revise.reject') }}" formmethod="POST" type="submit" class="btn btn-danger btn-block">{{ __('ui.ad.details.revise.reject') }}</button>
                            </div>
                        </div>    
                    </div>
                @else {{-- USER MODE --}}
                    <section class="row"> {{-- Sezione contatti --}}
                        <div class="col-12 col-lg-8 pl-4 pr-4 pr-lg-0">
                            <h2 class="font-weight-bold mb-4">{{ __('ui.ad.details.advertiser') }}</h2>
                        </div>

                        <div class="col-12 col-lg-8 pl-4 pr-4 pr-lg-0">
                            <div class="advertiser rounded p-3 mb-4">
                                <div class="row">
                                    <div class="col-12 col-md-6 mb-3">
                                        <div class="d-flex">
                                            <span class="first-letter">{{ substr($ad->user->name,0,1) }}</span>
                                            <div class="d-flex flex-column pl-4 pl-md-2">
                                                <span class="font-weight-bold">{{ $ad->user->name }}</span>
                                                <span>{{ __('ui.ad.details.info1') }} <span class="text-capitalize">{{ $ad->user->created_at->translatedFormat('F Y') }}</span></span>
                                            </div>
                                        </div> 
                                    </div>
                                
                                    <div class="col-12 col-md-6 d-flex flex-column pl-4 pl-md-3">
                                        <span class="ads-count font-weight-bold">{{ count($ad->user->ads) }}</span>
                                        <span>{{ __('ui.ad.details.info2') }}</span>
                                    </div>
                                </div>
                                <hr>
                                <button id="btn-contact" class="btn btn-lg btn-yellow btn-block"><i class="far fa-comment-dots"></i> {{ __('ui.ad.details.contact') }}</button>

                                <div id="contact-form" class="mt-3 p-3 py-4">
                                    <h2 class="font-weight-bold">{{ __('ui.ad.details.contact') }}</h2>
                                    <form action="" method="">
                                        <div class="form-group">
                                            <label class="font-weight-bold" for="message">{{ __('Messaggio') }}</label>
                                            <textarea class="form-control" id="message" rows="5">{{ __('Ciao') }} {{ $ad->user->name }}, {{ __('ti contatto per l\'annuncio') }} {{ $ad->title }}, </textarea>
                                        </div>
                                        <a role="button" href="" id="btn-contact-action" class="btn btn-lg btn-yellow btn-block"><i class="far fa-comment-dots"></i> {{ __('Invia messaggio') }}</a>
                                    </form>
                                </div>

                            </div>
                        </div>
                    </section>
                @endif

            </div>
        </article>
    </div>
@else
    <p class="pt-4 text-center">{{ __('ui.ad.details.no-revise') }}</p>
@endif

@endsection

@push('scripts')
<script>
document.addEventListener("DOMContentLoaded", function(){
    let btnContact = document.getElementById('btn-contact');

    if (btnContact){
        let contactForm = document.getElementById('contact-form');

        btnContact.addEventListener('click', function(){
            btnContact.parentNode.removeChild(btnContact);
            contactForm.classList.add('show');
            contactForm.scrollIntoView();
        });
    }
});
</script>
@endpush