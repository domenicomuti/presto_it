@extends('layouts.app')

@section('meta-title')
<title>{{ config('app.name', 'Laravel') }}: {{ __('404 - Page not found') }}</title>
@endsection

@section('content')

<h1 class="auth-title py-4">{{ __('404 - Page not found') }}</h1>

<div class="d-flex justify-content-center">
    <a href="/" class="btn btn-lg btn-yellow" tabindex="-1" role="button" aria-disabled="true">{{ __('ui.ad.insert.modify.back') }}</a>
</div>

@endsection