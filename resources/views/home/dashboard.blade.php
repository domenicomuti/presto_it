@extends('layouts.app')

@section('meta-title')
<title>{{ config('app.name', 'Laravel') }}: {{ __('ui.dashboard.title') }}</title>
@endsection

@section('content')

<div class="container dashboard">
    <div class="row d-none d-lg-block">
        <div class="col-12">
            <h2 class="mt-4 font-weight-bold">{{ __('ui.dashboard.welcome') }}</h2>
            <p>{{ __('ui.dashboard.head1') }}<br>{{ __('ui.dashboard.head2') }}</p>
        </div>
    </div>

    <div class="row top-bar py-3 by-lg-2 text-center mb-0 mb-lg-3">
        <div class="col-12 col-lg-4 offset-lg-8">
            <span class="d-none d-lg-block">{{ __('ui.dashboard.head3') }}<br>{{ __('ui.dashboard.head4') }}</span>
            <span class="d-lg-none">{{ __('ui.search.total') }} <span class="total">{{ count($ads) }}</span></span>
        </div>
    </div>

    @if (count($ads)==0)
        <div class="row text-center">
            <div class="col-12 pt-3 pt-lg-1">
                <p>{{ __('ui.search.no') }}</p>
                @include('includes.insert_ad_button')
            </div>
        </div>
    @else
        <div class="row ad-list px-0 px-lg-3 px-lg-0">
            <div class="col-12">

                {{-- Annunci --}}
                @foreach ($ads as $ad)
                    <div class="row ad p-3 mb-0 mb-lg-3">
                        <div class="col-4 col-lg-2 px-0 px-lg-3">
                            <a href="{{ route('ad_details', ['id'=>$ad->id,'slug'=>Str::slug($ad->title)]) }}">
                                <img class="w-100 image" src="{{ $ad->images->first() ? $ad->images->first()->getStorageUrl(300,300) : '/img/no-image.png' }}" alt="{{ $ad->title }}">
                            </a>
                        </div>
                        <div class="col-8 col-lg-10 d-flex flex-column">
                            <div class="2nd-col">
                                <a href="{{ route('ad_details', ['id'=>$ad->id,'slug'=>Str::slug($ad->title)]) }}">
                                    <span class="font-weight-bold m-0">{{ $ad->title }}</span>
                                </a>
                                <p class="price m-0">{{ $ad->price }} €</p>
                                <p class="date m-0">{{ $ad->created_at->translatedFormat('d/m/Y, H:i') }}</p>
                                @if ( $ad->created_at != $ad->updated_at)
                                    <p class="date m-0">{{ __('ui.dashboard.edited') }} {{ $ad->updated_at->translatedFormat('d/m/Y, H:i') }}</p>
                                @endif
                            </div>
                            <div class="last mt-3">
                                <a href="{{ route('home.modify_ad', $ad->id) }}" class="btn btn-link p-0 border-0">{{ __('ui.dashboard.edit') }}</a>

                                <span style="font-size: 1rem;">&nbsp;|&nbsp;</span>
                                
                                <button form="delete-ad-{{ $ad->id }}" type="submit" class="btn btn-link p-0 border-0">{{ __('ui.dashboard.delete') }}</button>
                            </div>
                            <form id="delete-ad-{{ $ad->id }}" action="{{ route('home.delete_ad', $ad->id) }}" method="POST">
                                @csrf
                                @method('DELETE')
                            </form>
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
    @endif
</div>

@endsection