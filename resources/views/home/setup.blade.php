@extends('layouts.app')

@section('meta-title')
<title>{{ config('app.name', 'Laravel') }}: setup</title>
@endsection

@section('content')

<div class="container">
    @if (session('warning'))
        <div class="alert alert-warning">
            {{ session('warning') }}
        </div>
    @endif

    <div class="row justify-content-center pt-4">
        <div class="col-10 col-lg-4">
            <form action="{{ route('home.setup.be_revisor', Auth::id()) }}" method="POST">
                @csrf
                @method("PUT")
                <button type="submit" class="btn btn-yellow px-3 btn-block">
                    <i class="fas fa-wrench pr-2"></i>
                    <span class="font-weight-bold">{{ __('ui.setup.be-revisor') }}</span>
                </button>
            </form>
        </div>
    </div>
</div>

@endsection