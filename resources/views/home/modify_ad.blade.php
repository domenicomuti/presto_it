@extends('layouts.app')

@section('meta-title')
<title>{{ config('app.name', 'Laravel') }}: {{ __('ui.ad.modify') }}</title>
@endsection

@section('content')

<div class="container">
    <div class="row justify-content-center">

        <div class="col-12 col-md-8 text-center mt-4 mb-2">
            <h2>{{ __('ui.ad.modify') }}</h2>
        </div>

        <div class="col-12 col-md-8">

        {{-- Errore validazione --}}
        @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
                </ul>
            </div>
        @endif

        {{-- Form --}}
        <form method="POST" action="{{ route('home.modify_ad.patch', ['id'=>$ad->id]) }}">
            @csrf
            @method('PATCH')
            
            <input id="temp" name="temp" value="{{ $temp }}" hidden>
            <input id="ad_id" name="ad_id" value="{{ $ad->id }}" hidden>

            <div class="container">

            {{-- Categoria --}}
            <div class="form-group">
                <label for="selectCategory"><strong>{{ __('ui.category') }}</strong></label>
                <select class="form-control" id="selectCategory" name="category_id">
                    @foreach($categories as $category_app)
                        <option {{ $ad->category->id==$category_app->id ? 'selected' : '' }} value="{{ $category_app->id }}">
                            @switch(App::getLocale())
                                @case('it') {{ $category_app->name }} @break
                                @case('en') {{ $category_app->name_eng }} @break
                                @case('es') {{ $category_app->name_esp }} @break
                            @endswitch
                        </option>
                    @endforeach
                </select>
            </div>
            
            {{-- Titolo --}}
            <div class="form-group">
                <label for="adTitle"><strong>{{ __('ui.title') }}</strong></label>
                <input type="text" class="form-control" name="title" id="adTitle" placeholder="{{ __('ui.title') }}" value="{{ $ad->title }}">
            </div>
        
            {{-- Descrizione --}}
            <div class="form-group">
                <label for="adDescription"><strong>{{ __('ui.description') }}</strong></label>
                <textarea id="adDescription" rows=6 class="form-control" name="description" placeholder="{{ __('ui.description.placeholder') }}">{{ $ad->description }}</textarea>
            </div>

            {{-- Prezzo --}}
            <div class="form-group">
                <label for="adPrice"><strong>{{ __('ui.price') }}</strong></label>
                <input id="adPrice" type="number" class="form-control" name="price" placeholder="{{ __('ui.price') }}" value={{ $ad->price }}>
            </div>

            {{-- Regione --}}
            <div class="form-group">
                <label for="adRegion"><strong>{{ __('ui.region') }}</strong></label>
                <select class="form-control" id="adRegion" name="region_id">
                    @foreach($regions as $region_app)
                        <option {{ $ad->region->id==$region_app->id ? 'selected' : '' }} value="{{ $region_app->id }}">{{ $region_app->name }}</option>
                    @endforeach
                </select>
            </div>

            {{-- Immagini --}}
            <div class="form-group mb-4">
                <label><strong>{{ __('ui.images') }}</strong> {{ __('ui.images.optional') }}</label>
                <div class="dropzone" id="dropzone-modify">
                    <div class="dz-message">{{ __('ui.images.placeholder') }}</div>
                </div>
            </div>

            <div id="tpl" hidden>
                <div class="dz-preview dz-file-preview">
                    <div class="dz-image">
                        <img data-dz-thumbnail="">
                    </div>
                    <div class="dz-details">
                        <div class="dz-size" data-dz-size></div>
                    </div>
                    <div class="dz-progress">
                        <span class="dz-upload" data-dz-uploadprogress></span>
                    </div>
                    <div class="dz-error-message">
                        <span data-dz-errormessage=""></span>
                    </div>
                    <div class="dz-success-mark">
                        <svg width="54px" height="54px" viewBox="0 0 54 54" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:sketch="http://www.bohemiancoding.com/sketch/ns">
                            <title>Check</title>
                            <defs></defs>
                            <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd" sketch:type="MSPage">
                                <path d="M23.5,31.8431458 L17.5852419,25.9283877 C16.0248253,24.3679711 13.4910294,24.366835 11.9289322,25.9289322 C10.3700136,27.4878508 10.3665912,30.0234455 11.9283877,31.5852419 L20.4147581,40.0716123 C20.5133999,40.1702541 20.6159315,40.2626649 20.7218615,40.3488435 C22.2835669,41.8725651 24.794234,41.8626202 26.3461564,40.3106978 L43.3106978,23.3461564 C44.8771021,21.7797521 44.8758057,19.2483887 43.3137085,17.6862915 C41.7547899,16.1273729 39.2176035,16.1255422 37.6538436,17.6893022 L23.5,31.8431458 Z M27,53 C41.3594035,53 53,41.3594035 53,27 C53,12.6405965 41.3594035,1 27,1 C12.6405965,1 1,12.6405965 1,27 C1,41.3594035 12.6405965,53 27,53 Z" stroke-opacity="0.198794158" stroke="#747474" fill-opacity="0.816519475" fill="#FFFFFF" sketch:type="MSShapeGroup"></path>
                            </g>
                        </svg>
                    </div>
                    <div class="dz-error-mark">
                        <svg width="54px" height="54px" viewBox="0 0 54 54" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:sketch="http://www.bohemiancoding.com/sketch/ns">
                            <title>Error</title>
                            <defs></defs>
                            <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd" sketch:type="MSPage">
                                <g sketch:type="MSLayerGroup" stroke="#747474" stroke-opacity="0.198794158" fill="#FFFFFF" fill-opacity="0.816519475">
                                    <path d="M32.6568542,29 L38.3106978,23.3461564 C39.8771021,21.7797521 39.8758057,19.2483887 38.3137085,17.6862915 C36.7547899,16.1273729 34.2176035,16.1255422 32.6538436,17.6893022 L27,23.3431458 L21.3461564,17.6893022 C19.7823965,16.1255422 17.2452101,16.1273729 15.6862915,17.6862915 C14.1241943,19.2483887 14.1228979,21.7797521 15.6893022,23.3461564 L21.3431458,29 L15.6893022,34.6538436 C14.1228979,36.2202479 14.1241943,38.7516113 15.6862915,40.3137085 C17.2452101,41.8726271 19.7823965,41.8744578 21.3461564,40.3106978 L27,34.6568542 L32.6538436,40.3106978 C34.2176035,41.8744578 36.7547899,41.8726271 38.3137085,40.3137085 C39.8758057,38.7516113 39.8771021,36.2202479 38.3106978,34.6538436 L32.6568542,29 Z M27,53 C41.3594035,53 53,41.3594035 53,27 C53,12.6405965 41.3594035,1 27,1 C12.6405965,1 1,12.6405965 1,27 C1,41.3594035 12.6405965,53 27,53 Z" sketch:type="MSShapeGroup"></path>
                                </g>
                            </g>
                        </svg>
                    </div>
                    <a class="dz-remove" data-dz-remove>{{ __('ui.dropzone.delete') }}</a>
                </div>
            </div>

            {{-- Submit --}}
            <button type="submit" class="btn btn-blue btn-block mb-5">{{ __('ui.dashboard.edit') }}</button>

            </div>
        </form>
        </div>
    </div>
</div>
@endsection

@push('scripts')
<script>
document.addEventListener('DOMContentLoaded', function(){
    let id = document.getElementById("ad_id").value;
    
    Echo.private(`ModifySessionExpired.${id}`)
        .listen("ModifySessionExpired", (e) => {
        
        temp = document.getElementById("temp").value;
        if (temp == e.temp){
            window.alert("{{ __('ui.session.expired') }}");
            window.location.href='/home';
        }
    });
});
</script>
@endpush