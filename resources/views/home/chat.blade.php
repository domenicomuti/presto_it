@extends('layouts.app')

@section('meta-title')
<title>{{ config('app.name', 'Laravel') }}: {{ strtolower(__('Messaggi')) }}</title>
@endsection

@section('content')

<div class="container chat">

    <h2 class="py-4 m-0 font-weight-bold">{{ __('Messaggi') }} (WORK IN PROGRESS)</h2>
    
    <div class="row chat-box mb-4 mb-md-0">

        {{-- Lista contatti --}}
        <div class="col-12 col-md-3 p-0 list">

            {{-- Contatti singoli --}}
            <div class="contact px-3 py-2" id="contact-1">
                <div class="row">
                    <div class="col-4 pr-0 image">
                        <img src="/img/placeholder.png" alt="" class="w-100 border rounded">
                    </div>
                    <div class="col-8">
                        <p class="m-0 name">Nome utente</p>
                        <p class="m-0 title">Titolo annuncio</p>
                        <p class="m-0 date">Data ultimo msg</p>
                    </div>
                </div>
            </div>

            {{-- Contatti singoli --}}
            <div class="contact px-3 py-2" id="contact-2">
                <div class="row">
                    <div class="col-4 pr-0 image">
                        <img src="/img/placeholder.png" alt="" class="w-100 border rounded">
                    </div>
                    <div class="col-8">
                        <p class="m-0 name">Nome utente</p>
                        <p class="m-0 title">Titolo annuncio</p>
                        <p class="m-0 date">Data ultimo msg</p>
                    </div>
                </div>
            </div>

            {{-- Contatti singoli --}}
            <div class="contact px-3 py-2" id="contact-3">
                <div class="row">
                    <div class="col-4 pr-0 image">
                        <img src="/img/placeholder.png" alt="" class="w-100 border rounded">
                    </div>
                    <div class="col-8">
                        <p class="m-0 name">Nome utente</p>
                        <p class="m-0 title">Titolo annuncio</p>
                        <p class="m-0 date">Data ultimo msg</p>
                    </div>
                </div>
            </div>

        </div>

        {{-- Finestra chat --}}
        <div class="col-9 p-0 chat-window">

            {{-- Message Box --}}
            <div class="messages active">
                <div class="head px-3 py-2">
                    <div class="row align-items-center">
                        <div class="col-5 col-md-7 username-head">Nome utente</div>
                        <div class="col-7 col-md-5 info text-right">
                            <div class="text mr-0 mr-md-3">
                                <span class="title-head">Titolo annuncio</span>
                                <span class="price">Prezzo</span>
                            </div>
                            <span class="d-flex align-items-center"><i class="fas fa-times chat-close d-md-none"></i></span>
                            <img src="/img/placeholder.png" alt="" class="border rounded d-none d-md-block">
                        </div>
                    </div>
                </div>
                <div class="content">
                    <span class="other">
                        contatto 1
                    </span>
                    <span class="you">
                        contatto 1
                    </span>
                    <span class="other">
                        contatto 1
                    </span>
                    <span class="you">
                        contatto 1
                    </span>
                    <span class="other">
                        contatto 1
                    </span>
                </div>
                <div class="input-text-box p-2">
                    <div contenteditable="true" class="textarea pr-1" placeholder="Scrivi il tuo messaggio..."></div>
                    <button type="submit" class="btn btn-blue m-1">Invia</button>
                </div>
            </div>

            {{-- Message Box --}}
            <div class="messages">
                <div class="head px-3 py-2">
                    <div class="row align-items-center">
                        <div class="col-5 col-md-7 username-head">Nome utente</div>
                        <div class="col-7 col-md-5 info text-right">
                            <div class="text mr-0 mr-md-3">
                                <span class="title-head">Titolo annuncio</span>
                                <span class="price">Prezzo</span>
                            </div>
                            <span class="d-flex align-items-center"><i class="fas fa-times chat-close d-md-none"></i></span>
                            <img src="/img/placeholder.png" alt="" class="border rounded d-none d-md-block">
                        </div>
                    </div>
                </div>
                <div class="content">
                    <span class="other">
                        contatto 2
                    </span>
                    <span class="you">
                        contatto 2
                    </span>
                    <span class="other">
                        contatto 2
                    </span>
                    <span class="you">
                        contatto 2
                    </span>
                    <span class="other">
                        contatto 2
                    </span>
                </div>
                <div class="input-text-box p-2">
                    <div contenteditable="true" class="textarea pr-1" placeholder="Scrivi il tuo messaggio..."></div>
                    <button type="submit" class="btn btn-blue m-1">Invia</button>
                </div>
            </div>

            {{-- Message Box --}}
            <div class="messages">
                <div class="head px-3 py-2">
                    <div class="row align-items-center">
                        <div class="col-5 col-md-7 username-head">Nome utente</div>
                        <div class="col-7 col-md-5 info text-right">
                            <div class="text mr-0 mr-md-3">
                                <span class="title-head">Titolo annuncio</span>
                                <span class="price">Prezzo</span>
                            </div>
                            <span class="d-flex align-items-center"><i class="fas fa-times chat-close d-md-none"></i></span>
                            <img src="/img/placeholder.png" alt="" class="border rounded d-none d-md-block">
                        </div>
                    </div>
                </div>
                <div class="content">
                    <span class="other">
                        contatto 3
                    </span>
                    <span class="you">
                        contatto 3
                    </span>
                    <span class="other">
                        contatto 3
                    </span>
                    <span class="you">
                        contatto 3
                    </span>
                    <span class="other">
                        contatto 3
                    </span>
                </div>
                <div class="input-text-box p-2">
                    <div contenteditable="true" class="textarea pr-1" placeholder="Scrivi il tuo messaggio..."></div>
                    <button type="submit" class="btn btn-blue m-1">Invia</button>
                </div>
            </div>

        </div>

    </div>
    
</div>

@endsection