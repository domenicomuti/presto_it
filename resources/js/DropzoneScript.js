document.addEventListener("DOMContentLoaded", function(){

    // UNOBTRUSIVE JS
    if (document.getElementById('dropzone')){

        let csrfToken = document.querySelector("meta[name='csrf-token']").getAttribute('content');
        let secret = document.querySelector("input[name='secret']").getAttribute('value');

        let myDropzone = new Dropzone("#dropzone", {
            previewTemplate: document.getElementById('tpl').innerHTML,
            url: "/insert/add-img",
            acceptedFiles: 'image/jpeg, image/png',
            maxFiles: 5,
            maxFilesize: 2,
            params: {
                _token: csrfToken,
                secret: secret
            },
            init: function(){
                // CHIAMATA AJAX PER CONTROLLARE SE DIRECTORY ESISTE GIA'
                let xhttp = new XMLHttpRequest();
                xhttp.open("GET", `/insert/check-img?secret=${secret}`, true);
                xhttp.send();
                xhttp.onreadystatechange = function() {
                    if (this.readyState == 4 && this.status == 200) {
                        response = JSON.parse(this.response);
                        if (response.exists){
                            response.images.forEach(function(image){
                                if(!image.filename.match(/(crop120x120_){1}[\s\S]+/)){
                                    myDropzone.options.addRemoveLinks = false;
                                    myDropzone.options.maxFiles = myDropzone.options.maxFiles - 1;
                                    let mockFile = { storageName: response.url+'/'+image.filename, size: image.size };
                                    myDropzone.emit("addedfile",mockFile);
                                    myDropzone.emit("thumbnail", mockFile, response.storageUrl+'/crop120x120_'+image.filename);
                                    myDropzone.emit("complete", mockFile);
                                }  
                            });
                        }
                    }
                };
            }
        });

        myDropzone.on('success', function(file,response){
            file.storageName = response;
        });

        // CHIAMATA AJAX PER LA RIMOZIONE DEI FILE
        myDropzone.on('removedfile', function(file){
            let xhttp = new XMLHttpRequest();
            xhttp.open("POST", "/insert/delete-img", true);
            xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
            xhttp.send(`_token=${csrfToken}&secret=${secret}&_method=DELETE&filename=${file.storageName}`);
        });
    }
});