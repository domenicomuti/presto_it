document.addEventListener("DOMContentLoaded", function(){

    // UNOBTRUSIVE JS
    if (document.getElementById('dropzone-modify')){

        let csrfToken = document.querySelector("meta[name='csrf-token']").getAttribute('content');
        let ad_id = document.getElementById("ad_id").value;
        let temp = document.getElementById("temp").value;

        let myDropzone = new Dropzone("#dropzone-modify", {
            previewTemplate: document.getElementById('tpl').innerHTML,
            url: `/home/modify-ad/${ad_id}/add-img`,
            acceptedFiles: 'image/jpeg, image/png',
            maxFiles: 5,
            maxFilesize: 2,
            params: {
                _token: csrfToken,
                temp: temp
            },
            init: function() {
                // CHIAMATA AJAX PER CONTROLLARE SE DIRECTORY ESISTE GIA'
                let xhttp = new XMLHttpRequest();
                xhttp.open("GET", `/home/modify-ad/${ad_id}/check-img`, true);
                xhttp.send();
                xhttp.onreadystatechange = function() {
                    if (this.readyState == 4 && this.status == 200) {
                        response = JSON.parse(this.response);
                        if (response.exists){
                            response.images.forEach(function(image) {
                                let mockFile = { storageName: response.url+'/'+image.filename, size: image.size };
                                myDropzone.emit("addedfile",mockFile);
                                myDropzone.emit("thumbnail", mockFile, response.storageUrl+'/crop120x120_'+image.filename);
                                myDropzone.emit("complete", mockFile);
                                myDropzone.options.maxFiles = myDropzone.options.maxFiles - 1;
                            });
                        }
                    }
                };
            }
        });

        myDropzone.on('success', function(file,response){
            file.storageName = response;
        });

        // CHIAMATA AJAX PER LA RIMOZIONE DEI FILE
        myDropzone.on('removedfile', function(file){
            let xhttp = new XMLHttpRequest();
            xhttp.open("POST", `/home/modify-ad/${ad_id}/delete-img`, true);
            xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
            xhttp.send(`_token=${csrfToken}&_id=${ad_id}&_method=DELETE&filename=${file.storageName}`);
            xhttp.onreadystatechange = function() {
                if (this.readyState == 4 && this.status == 200) {
                    response = JSON.parse(this.response)
                    orig = response.orig;
                    myDropzone.options.maxFiles = 5 - orig;
                }
            }
        });
    }
});