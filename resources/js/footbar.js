document.addEventListener("DOMContentLoaded", function(){
    let footbar = document.getElementById("footbar");
    let body = document.querySelector('body');

    document.querySelectorAll(".footbar-open").forEach(e => {
        e.onclick = function(){
            footbar.classList.add('active');
            body.classList.add('no-scroll');
        };
    });

    document.getElementById("footbar-close").onclick = function(){
        footbar.classList.remove('active');
        body.classList.remove('no-scroll');
    };
});