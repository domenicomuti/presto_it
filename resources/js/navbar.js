document.addEventListener("DOMContentLoaded", function(){
    let navbar = document.querySelector('.navbar');
    let navbar_height = 48;

    let scroll_history = [];
    scroll_history.push(window.pageYOffset);
    
    window.addEventListener("scroll", function(){  
        if (window.pageYOffset > navbar_height){
            navbar.classList.add('hide');
            scroll_history.push(window.pageYOffset);

            if (scroll_history.length > 2){
                scroll_history.shift(window.pageYOffset);
                if (scroll_history[0] > scroll_history[1]) {
                    navbar.classList.remove('hide');
                }
            }
        }
    });
});