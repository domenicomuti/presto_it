document.addEventListener("DOMContentLoaded", function(){
    let sidebar = document.getElementById('sidebar');
    let background = document.getElementById('bg-layer');
    let body = document.querySelector('body');

    function sidebarClose(){
        sidebar.classList.remove('active');
        background.classList.remove('active');
        body.classList.remove('no-scroll');
    };

    document.getElementById('sidebar-open').onclick = function(){
        sidebar.classList.add('active');
        background.classList.add('active');
        body.classList.add('no-scroll');
    };

    document.getElementById('sidebar-close').onclick = function(){
        sidebarClose();
    };

    background.onclick = function(){
        sidebarClose();
    };
});