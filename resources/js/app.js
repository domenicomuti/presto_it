// Importa file localizzazione
en = require('../lang/en.json');
it = require('../lang/it.json');
es = require('../lang/es.json');

// Imposta la lingua
let lang = document.querySelector('html').getAttribute('lang');
switch (lang){
    case 'it':
        lang = it;
        break;
    case 'en':
        lang = en;
        break;
    case 'es':
        lang = es;
        break;
}
window.lang = lang;

// Fontawesome
require('@fortawesome/fontawesome-free');

require('./bootstrap');

require('./navbar.js');
require('./sidebar.js');
require('./footbar.js');
require('./set-locale.js');
require('./favorites.js');
require('./chat.js');

// Dropzone
window.Dropzone = require('dropzone');
Dropzone.autoDiscover = false;
require('./DropzoneScript');
require('./DropzoneModifyScript.js');

// Slick Carousel
require('slick-carousel');
require('./my-slick-carousel.js');