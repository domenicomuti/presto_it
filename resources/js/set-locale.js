document.addEventListener("DOMContentLoaded", function(){
    dropdown = document.querySelector('.set-locale .dropdown');
    document.addEventListener('click', (e)=>{
        if (e.target.closest('.set-locale')){
            if (dropdown.classList.contains('active')){
                dropdown.classList.remove('active');
            }else{
                dropdown.classList.add('active');
            }
        }else{
            dropdown.classList.remove('active');
        }
    });
});