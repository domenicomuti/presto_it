document.addEventListener('DOMContentLoaded', function(){

    function ajaxAdd(xhttp, user_id, ad_id){
        xhttp.open("POST", `/favorite/add`, true);
        xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
        xhttp.send(`_token=${csrfToken}&user_id=${user_id}&ad_id=${ad_id}`);
    }

    function ajaxDelete(xhttp, user_id, ad_id){
        xhttp.open("POST", `/favorite/delete`, true);
        xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
        xhttp.send(`_token=${csrfToken}&_method=DELETE&user_id=${user_id}&ad_id=${ad_id}`);
    }

    let csrfToken = document.querySelector("meta[name='csrf-token']").getAttribute('content');
    let actions = document.getElementsByClassName('favorites');

    if (actions){
        for (action of actions){
            let user_id = action.getAttribute('data-user-id');
            let ad_id = action.getAttribute('data-ad-id');
            let button = action.querySelector('button');
    
            let xhttp = new XMLHttpRequest();

            let icon = document.querySelector(`#icon-ad-${ad_id}`);
            let msg = document.querySelector(`#msg-fav-${ad_id}`);

            let total = document.querySelector('.total');

            button.addEventListener('click', function(){
                if (!icon){    
                    total.innerHTML = total.innerHTML - 1;
                    ajaxDelete(xhttp, user_id, ad_id);
                    document.getElementById(`ad-${ad_id}`).classList.add('deleted');
                }else{
                    if (icon.classList.contains('far')){
                        ajaxAdd(xhttp, user_id, ad_id);
    
                        icon.classList.remove('far');
                        icon.classList.add('fas');
    
                        if (msg){
                            msg.innerHTML = lang['Togli dai Preferiti'];
                        }
                    }else{
                        ajaxDelete(xhttp, user_id, ad_id);
    
                        icon.classList.remove('fas');
                        icon.classList.add('far');
                        
                        if (msg){
                            msg.innerHTML = lang['Metti tra i Preferiti'];
                        }
                    }
                }
            });
        }
    }
});