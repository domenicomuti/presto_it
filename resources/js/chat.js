document.addEventListener("DOMContentLoaded", function(){
    if(document.querySelector('.container .chat')){
        let contactsButtons = document.querySelectorAll('.list .contact');
        let messageBoxes = document.querySelectorAll('.messages');
        let closeIcons = document.querySelectorAll('.chat-close');
        let chatWindow = document.querySelector('.chat-window');
        let body = document.querySelector('body');

        if (window.outerWidth >= 768) { contactsButtons[0].classList.add('active'); };

        contactsButtons.forEach((contactButton, key) => {
            contactButton.addEventListener('click', function(){
                let lastActiveContact = document.querySelector('.list .contact.active');
                let lastActiveMessageBox = document.querySelector('.messages.active');
                if (lastActiveContact) { lastActiveContact.classList.remove('active'); };
                if (lastActiveMessageBox) { lastActiveMessageBox.classList.remove('active'); };

                contactButton.classList.add('active');
                messageBoxes[key].classList.add('active');
                chatWindow.classList.add('show-on-smartphone');
                body.classList.add('no-scroll');
            });

            if (closeIcons){
                closeIcons[key].addEventListener('click', function(){
                    chatWindow.classList.remove('show-on-smartphone');
                    body.classList.remove('no-scroll');
                });
            }
        });
    }
});