document.addEventListener("DOMContentLoaded", function(){
    if (document.querySelector('.slick-bottom')){
        $('.slick-carousel').slick({
            dots: true,
            prevArrow: '<i class="fas fa-arrow-left d-none d-md-block"></i>',
            nextArrow: '<i class="fas fa-arrow-right d-none d-md-block"></i>',
            asNavFor: '.slick-bottom'
        });
        $('.slick-bottom').slick({
            arrows: false,
            draggable: false,
            swipe: false
        });
    }else{
        $('.slick-carousel').slick({
            dots: true,
            prevArrow: '<i class="fas fa-arrow-left d-none d-md-block"></i>',
            nextArrow: '<i class="fas fa-arrow-right d-none d-md-block"></i>'
        });
    }

});